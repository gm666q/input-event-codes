#[macro_export]
macro_rules! MSC_SERIAL {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! MSC_PULSELED {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! MSC_GESTURE {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! MSC_RAW {
	() => {
		0x03
	};
}
#[macro_export]
macro_rules! MSC_SCAN {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! MSC_TIMESTAMP {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! MSC_MAX {
	() => {
		0x07
	};
}
#[macro_export]
macro_rules! MSC_CNT {
	() => {
		($crate::MSC_MAX!() + 1)
	};
}
