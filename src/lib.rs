//! Linux input event codes form `linux/input_event_codes.h`.
#![allow(non_snake_case)]
#![cfg(any(target_os = "android", target_os = "linux"))]
#![no_std]

mod abs;
mod ev;
mod input_prop;
mod key;
mod led;
mod msc;
mod rel;
mod rep;
mod snd;
mod sw;
mod syn;
