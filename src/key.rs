#[macro_export]
macro_rules! KEY_RESERVED {
	() => {
		0
	};
}
#[macro_export]
macro_rules! KEY_ESC {
	() => {
		1
	};
}
#[macro_export]
macro_rules! KEY_1 {
	() => {
		2
	};
}
#[macro_export]
macro_rules! KEY_2 {
	() => {
		3
	};
}
#[macro_export]
macro_rules! KEY_3 {
	() => {
		4
	};
}
#[macro_export]
macro_rules! KEY_4 {
	() => {
		5
	};
}
#[macro_export]
macro_rules! KEY_5 {
	() => {
		6
	};
}
#[macro_export]
macro_rules! KEY_6 {
	() => {
		7
	};
}
#[macro_export]
macro_rules! KEY_7 {
	() => {
		8
	};
}
#[macro_export]
macro_rules! KEY_8 {
	() => {
		9
	};
}
#[macro_export]
macro_rules! KEY_9 {
	() => {
		10
	};
}
#[macro_export]
macro_rules! KEY_0 {
	() => {
		11
	};
}
#[macro_export]
macro_rules! KEY_MINUS {
	() => {
		12
	};
}
#[macro_export]
macro_rules! KEY_EQUAL {
	() => {
		13
	};
}
#[macro_export]
macro_rules! KEY_BACKSPACE {
	() => {
		14
	};
}
#[macro_export]
macro_rules! KEY_TAB {
	() => {
		15
	};
}
#[macro_export]
macro_rules! KEY_Q {
	() => {
		16
	};
}
#[macro_export]
macro_rules! KEY_W {
	() => {
		17
	};
}
#[macro_export]
macro_rules! KEY_E {
	() => {
		18
	};
}
#[macro_export]
macro_rules! KEY_R {
	() => {
		19
	};
}
#[macro_export]
macro_rules! KEY_T {
	() => {
		20
	};
}
#[macro_export]
macro_rules! KEY_Y {
	() => {
		21
	};
}
#[macro_export]
macro_rules! KEY_U {
	() => {
		22
	};
}
#[macro_export]
macro_rules! KEY_I {
	() => {
		23
	};
}
#[macro_export]
macro_rules! KEY_O {
	() => {
		24
	};
}
#[macro_export]
macro_rules! KEY_P {
	() => {
		25
	};
}
#[macro_export]
macro_rules! KEY_LEFTBRACE {
	() => {
		26
	};
}
#[macro_export]
macro_rules! KEY_RIGHTBRACE {
	() => {
		27
	};
}
#[macro_export]
macro_rules! KEY_ENTER {
	() => {
		28
	};
}
#[macro_export]
macro_rules! KEY_LEFTCTRL {
	() => {
		29
	};
}
#[macro_export]
macro_rules! KEY_A {
	() => {
		30
	};
}
#[macro_export]
macro_rules! KEY_S {
	() => {
		31
	};
}
#[macro_export]
macro_rules! KEY_D {
	() => {
		32
	};
}
#[macro_export]
macro_rules! KEY_F {
	() => {
		33
	};
}
#[macro_export]
macro_rules! KEY_G {
	() => {
		34
	};
}
#[macro_export]
macro_rules! KEY_H {
	() => {
		35
	};
}
#[macro_export]
macro_rules! KEY_J {
	() => {
		36
	};
}
#[macro_export]
macro_rules! KEY_K {
	() => {
		37
	};
}
#[macro_export]
macro_rules! KEY_L {
	() => {
		38
	};
}
#[macro_export]
macro_rules! KEY_SEMICOLON {
	() => {
		39
	};
}
#[macro_export]
macro_rules! KEY_APOSTROPHE {
	() => {
		40
	};
}
#[macro_export]
macro_rules! KEY_GRAVE {
	() => {
		41
	};
}
#[macro_export]
macro_rules! KEY_LEFTSHIFT {
	() => {
		42
	};
}
#[macro_export]
macro_rules! KEY_BACKSLASH {
	() => {
		43
	};
}
#[macro_export]
macro_rules! KEY_Z {
	() => {
		44
	};
}
#[macro_export]
macro_rules! KEY_X {
	() => {
		45
	};
}
#[macro_export]
macro_rules! KEY_C {
	() => {
		46
	};
}
#[macro_export]
macro_rules! KEY_V {
	() => {
		47
	};
}
#[macro_export]
macro_rules! KEY_B {
	() => {
		48
	};
}
#[macro_export]
macro_rules! KEY_N {
	() => {
		49
	};
}
#[macro_export]
macro_rules! KEY_M {
	() => {
		50
	};
}
#[macro_export]
macro_rules! KEY_COMMA {
	() => {
		51
	};
}
#[macro_export]
macro_rules! KEY_DOT {
	() => {
		52
	};
}
#[macro_export]
macro_rules! KEY_SLASH {
	() => {
		53
	};
}
#[macro_export]
macro_rules! KEY_RIGHTSHIFT {
	() => {
		54
	};
}
#[macro_export]
macro_rules! KEY_KPASTERISK {
	() => {
		55
	};
}
#[macro_export]
macro_rules! KEY_LEFTALT {
	() => {
		56
	};
}
#[macro_export]
macro_rules! KEY_SPACE {
	() => {
		57
	};
}
#[macro_export]
macro_rules! KEY_CAPSLOCK {
	() => {
		58
	};
}
#[macro_export]
macro_rules! KEY_F1 {
	() => {
		59
	};
}
#[macro_export]
macro_rules! KEY_F2 {
	() => {
		60
	};
}
#[macro_export]
macro_rules! KEY_F3 {
	() => {
		61
	};
}
#[macro_export]
macro_rules! KEY_F4 {
	() => {
		62
	};
}
#[macro_export]
macro_rules! KEY_F5 {
	() => {
		63
	};
}
#[macro_export]
macro_rules! KEY_F6 {
	() => {
		64
	};
}
#[macro_export]
macro_rules! KEY_F7 {
	() => {
		65
	};
}
#[macro_export]
macro_rules! KEY_F8 {
	() => {
		66
	};
}
#[macro_export]
macro_rules! KEY_F9 {
	() => {
		67
	};
}
#[macro_export]
macro_rules! KEY_F10 {
	() => {
		68
	};
}
#[macro_export]
macro_rules! KEY_NUMLOCK {
	() => {
		69
	};
}
#[macro_export]
macro_rules! KEY_SCROLLLOCK {
	() => {
		70
	};
}
#[macro_export]
macro_rules! KEY_KP7 {
	() => {
		71
	};
}
#[macro_export]
macro_rules! KEY_KP8 {
	() => {
		72
	};
}
#[macro_export]
macro_rules! KEY_KP9 {
	() => {
		73
	};
}
#[macro_export]
macro_rules! KEY_KPMINUS {
	() => {
		74
	};
}
#[macro_export]
macro_rules! KEY_KP4 {
	() => {
		75
	};
}
#[macro_export]
macro_rules! KEY_KP5 {
	() => {
		76
	};
}
#[macro_export]
macro_rules! KEY_KP6 {
	() => {
		77
	};
}
#[macro_export]
macro_rules! KEY_KPPLUS {
	() => {
		78
	};
}
#[macro_export]
macro_rules! KEY_KP1 {
	() => {
		79
	};
}
#[macro_export]
macro_rules! KEY_KP2 {
	() => {
		80
	};
}
#[macro_export]
macro_rules! KEY_KP3 {
	() => {
		81
	};
}
#[macro_export]
macro_rules! KEY_KP0 {
	() => {
		82
	};
}
#[macro_export]
macro_rules! KEY_KPDOT {
	() => {
		83
	};
}

#[macro_export]
macro_rules! KEY_ZENKAKUHANKAKU {
	() => {
		85
	};
}
#[macro_export]
macro_rules! KEY_102ND {
	() => {
		86
	};
}
#[macro_export]
macro_rules! KEY_F11 {
	() => {
		87
	};
}
#[macro_export]
macro_rules! KEY_F12 {
	() => {
		88
	};
}
#[macro_export]
macro_rules! KEY_RO {
	() => {
		89
	};
}
#[macro_export]
macro_rules! KEY_KATAKANA {
	() => {
		90
	};
}
#[macro_export]
macro_rules! KEY_HIRAGANA {
	() => {
		91
	};
}
#[macro_export]
macro_rules! KEY_HENKAN {
	() => {
		92
	};
}
#[macro_export]
macro_rules! KEY_KATAKANAHIRAGANA {
	() => {
		93
	};
}
#[macro_export]
macro_rules! KEY_MUHENKAN {
	() => {
		94
	};
}
#[macro_export]
macro_rules! KEY_KPJPCOMMA {
	() => {
		95
	};
}
#[macro_export]
macro_rules! KEY_KPENTER {
	() => {
		96
	};
}
#[macro_export]
macro_rules! KEY_RIGHTCTRL {
	() => {
		97
	};
}
#[macro_export]
macro_rules! KEY_KPSLASH {
	() => {
		98
	};
}
#[macro_export]
macro_rules! KEY_SYSRQ {
	() => {
		99
	};
}
#[macro_export]
macro_rules! KEY_RIGHTALT {
	() => {
		100
	};
}
#[macro_export]
macro_rules! KEY_LINEFEED {
	() => {
		101
	};
}
#[macro_export]
macro_rules! KEY_HOME {
	() => {
		102
	};
}
#[macro_export]
macro_rules! KEY_UP {
	() => {
		103
	};
}
#[macro_export]
macro_rules! KEY_PAGEUP {
	() => {
		104
	};
}
#[macro_export]
macro_rules! KEY_LEFT {
	() => {
		105
	};
}
#[macro_export]
macro_rules! KEY_RIGHT {
	() => {
		106
	};
}
#[macro_export]
macro_rules! KEY_END {
	() => {
		107
	};
}
#[macro_export]
macro_rules! KEY_DOWN {
	() => {
		108
	};
}
#[macro_export]
macro_rules! KEY_PAGEDOWN {
	() => {
		109
	};
}
#[macro_export]
macro_rules! KEY_INSERT {
	() => {
		110
	};
}
#[macro_export]
macro_rules! KEY_DELETE {
	() => {
		111
	};
}
#[macro_export]
macro_rules! KEY_MACRO {
	() => {
		112
	};
}
#[macro_export]
macro_rules! KEY_MUTE {
	() => {
		113
	};
}
#[macro_export]
macro_rules! KEY_VOLUMEDOWN {
	() => {
		114
	};
}
#[macro_export]
macro_rules! KEY_VOLUMEUP {
	() => {
		115
	};
}
#[macro_export]
macro_rules! KEY_POWER {
	() => {
		116
	};
}
#[macro_export]
macro_rules! KEY_KPEQUAL {
	() => {
		117
	};
}
#[macro_export]
macro_rules! KEY_KPPLUSMINUS {
	() => {
		118
	};
}
#[macro_export]
macro_rules! KEY_PAUSE {
	() => {
		119
	};
}
#[macro_export]
macro_rules! KEY_SCALE {
	() => {
		120
	};
}

#[macro_export]
macro_rules! KEY_KPCOMMA {
	() => {
		121
	};
}
#[macro_export]
macro_rules! KEY_HANGEUL {
	() => {
		122
	};
}
#[macro_export]
macro_rules! KEY_HANGUEL {
	() => {
		$crate::KEY_HANGEUL!()
	};
}
#[macro_export]
macro_rules! KEY_HANJA {
	() => {
		123
	};
}
#[macro_export]
macro_rules! KEY_YEN {
	() => {
		124
	};
}
#[macro_export]
macro_rules! KEY_LEFTMETA {
	() => {
		125
	};
}
#[macro_export]
macro_rules! KEY_RIGHTMETA {
	() => {
		126
	};
}
#[macro_export]
macro_rules! KEY_COMPOSE {
	() => {
		127
	};
}

#[macro_export]
macro_rules! KEY_STOP {
	() => {
		128
	};
}
#[macro_export]
macro_rules! KEY_AGAIN {
	() => {
		129
	};
}
#[macro_export]
macro_rules! KEY_PROPS {
	() => {
		130
	};
}
#[macro_export]
macro_rules! KEY_UNDO {
	() => {
		131
	};
}
#[macro_export]
macro_rules! KEY_FRONT {
	() => {
		132
	};
}
#[macro_export]
macro_rules! KEY_COPY {
	() => {
		133
	};
}
#[macro_export]
macro_rules! KEY_OPEN {
	() => {
		134
	};
}
#[macro_export]
macro_rules! KEY_PASTE {
	() => {
		135
	};
}
#[macro_export]
macro_rules! KEY_FIND {
	() => {
		136
	};
}
#[macro_export]
macro_rules! KEY_CUT {
	() => {
		137
	};
}
#[macro_export]
macro_rules! KEY_HELP {
	() => {
		138
	};
}
#[macro_export]
macro_rules! KEY_MENU {
	() => {
		139
	};
}
#[macro_export]
macro_rules! KEY_CALC {
	() => {
		140
	};
}
#[macro_export]
macro_rules! KEY_SETUP {
	() => {
		141
	};
}
#[macro_export]
macro_rules! KEY_SLEEP {
	() => {
		142
	};
}
#[macro_export]
macro_rules! KEY_WAKEUP {
	() => {
		143
	};
}
#[macro_export]
macro_rules! KEY_FILE {
	() => {
		144
	};
}
#[macro_export]
macro_rules! KEY_SENDFILE {
	() => {
		145
	};
}
#[macro_export]
macro_rules! KEY_DELETEFILE {
	() => {
		146
	};
}
#[macro_export]
macro_rules! KEY_XFER {
	() => {
		147
	};
}
#[macro_export]
macro_rules! KEY_PROG1 {
	() => {
		148
	};
}
#[macro_export]
macro_rules! KEY_PROG2 {
	() => {
		149
	};
}
#[macro_export]
macro_rules! KEY_WWW {
	() => {
		150
	};
}
#[macro_export]
macro_rules! KEY_MSDOS {
	() => {
		151
	};
}
#[macro_export]
macro_rules! KEY_COFFEE {
	() => {
		152
	};
}
#[macro_export]
macro_rules! KEY_SCREENLOCK {
	() => {
		$crate::KEY_COFFEE!()
	};
}
#[macro_export]
macro_rules! KEY_ROTATE_DISPLAY {
	() => {
		153
	};
}
#[macro_export]
macro_rules! KEY_DIRECTION {
	() => {
		$crate::KEY_ROTATE_DISPLAY!()
	};
}
#[macro_export]
macro_rules! KEY_CYCLEWINDOWS {
	() => {
		154
	};
}
#[macro_export]
macro_rules! KEY_MAIL {
	() => {
		155
	};
}
#[macro_export]
macro_rules! KEY_BOOKMARKS {
	() => {
		156
	};
}
#[macro_export]
macro_rules! KEY_COMPUTER {
	() => {
		157
	};
}
#[macro_export]
macro_rules! KEY_BACK {
	() => {
		158
	};
}
#[macro_export]
macro_rules! KEY_FORWARD {
	() => {
		159
	};
}
#[macro_export]
macro_rules! KEY_CLOSECD {
	() => {
		160
	};
}
#[macro_export]
macro_rules! KEY_EJECTCD {
	() => {
		161
	};
}
#[macro_export]
macro_rules! KEY_EJECTCLOSECD {
	() => {
		162
	};
}
#[macro_export]
macro_rules! KEY_NEXTSONG {
	() => {
		163
	};
}
#[macro_export]
macro_rules! KEY_PLAYPAUSE {
	() => {
		164
	};
}
#[macro_export]
macro_rules! KEY_PREVIOUSSONG {
	() => {
		165
	};
}
#[macro_export]
macro_rules! KEY_STOPCD {
	() => {
		166
	};
}
#[macro_export]
macro_rules! KEY_RECORD {
	() => {
		167
	};
}
#[macro_export]
macro_rules! KEY_REWIND {
	() => {
		168
	};
}
#[macro_export]
macro_rules! KEY_PHONE {
	() => {
		169
	};
}
#[macro_export]
macro_rules! KEY_ISO {
	() => {
		170
	};
}
#[macro_export]
macro_rules! KEY_CONFIG {
	() => {
		171
	};
}
#[macro_export]
macro_rules! KEY_HOMEPAGE {
	() => {
		172
	};
}
#[macro_export]
macro_rules! KEY_REFRESH {
	() => {
		173
	};
}
#[macro_export]
macro_rules! KEY_EXIT {
	() => {
		174
	};
}
#[macro_export]
macro_rules! KEY_MOVE {
	() => {
		175
	};
}
#[macro_export]
macro_rules! KEY_EDIT {
	() => {
		176
	};
}
#[macro_export]
macro_rules! KEY_SCROLLUP {
	() => {
		177
	};
}
#[macro_export]
macro_rules! KEY_SCROLLDOWN {
	() => {
		178
	};
}
#[macro_export]
macro_rules! KEY_KPLEFTPAREN {
	() => {
		179
	};
}
#[macro_export]
macro_rules! KEY_KPRIGHTPAREN {
	() => {
		180
	};
}
#[macro_export]
macro_rules! KEY_NEW {
	() => {
		181
	};
}
#[macro_export]
macro_rules! KEY_REDO {
	() => {
		182
	};
}

#[macro_export]
macro_rules! KEY_F13 {
	() => {
		183
	};
}
#[macro_export]
macro_rules! KEY_F14 {
	() => {
		184
	};
}
#[macro_export]
macro_rules! KEY_F15 {
	() => {
		185
	};
}
#[macro_export]
macro_rules! KEY_F16 {
	() => {
		186
	};
}
#[macro_export]
macro_rules! KEY_F17 {
	() => {
		187
	};
}
#[macro_export]
macro_rules! KEY_F18 {
	() => {
		188
	};
}
#[macro_export]
macro_rules! KEY_F19 {
	() => {
		189
	};
}
#[macro_export]
macro_rules! KEY_F20 {
	() => {
		190
	};
}
#[macro_export]
macro_rules! KEY_F21 {
	() => {
		191
	};
}
#[macro_export]
macro_rules! KEY_F22 {
	() => {
		192
	};
}
#[macro_export]
macro_rules! KEY_F23 {
	() => {
		193
	};
}
#[macro_export]
macro_rules! KEY_F24 {
	() => {
		194
	};
}

#[macro_export]
macro_rules! KEY_PLAYCD {
	() => {
		200
	};
}
#[macro_export]
macro_rules! KEY_PAUSECD {
	() => {
		201
	};
}
#[macro_export]
macro_rules! KEY_PROG3 {
	() => {
		202
	};
}
#[macro_export]
macro_rules! KEY_PROG4 {
	() => {
		203
	};
}
#[macro_export]
macro_rules! KEY_ALL_APPLICATIONS {
	() => {
		204
	};
}
#[macro_export]
macro_rules! KEY_DASHBOARD {
	() => {
		$crate::KEY_ALL_APPLICATIONS!()
	};
}
#[macro_export]
macro_rules! KEY_SUSPEND {
	() => {
		205
	};
}
#[macro_export]
macro_rules! KEY_CLOSE {
	() => {
		206
	};
}
#[macro_export]
macro_rules! KEY_PLAY {
	() => {
		207
	};
}
#[macro_export]
macro_rules! KEY_FASTFORWARD {
	() => {
		208
	};
}
#[macro_export]
macro_rules! KEY_BASSBOOST {
	() => {
		209
	};
}
#[macro_export]
macro_rules! KEY_PRINT {
	() => {
		210
	};
}
#[macro_export]
macro_rules! KEY_HP {
	() => {
		211
	};
}
#[macro_export]
macro_rules! KEY_CAMERA {
	() => {
		212
	};
}
#[macro_export]
macro_rules! KEY_SOUND {
	() => {
		213
	};
}
#[macro_export]
macro_rules! KEY_QUESTION {
	() => {
		214
	};
}
#[macro_export]
macro_rules! KEY_EMAIL {
	() => {
		215
	};
}
#[macro_export]
macro_rules! KEY_CHAT {
	() => {
		216
	};
}
#[macro_export]
macro_rules! KEY_SEARCH {
	() => {
		217
	};
}
#[macro_export]
macro_rules! KEY_CONNECT {
	() => {
		218
	};
}
#[macro_export]
macro_rules! KEY_FINANCE {
	() => {
		219
	};
}
#[macro_export]
macro_rules! KEY_SPORT {
	() => {
		220
	};
}
#[macro_export]
macro_rules! KEY_SHOP {
	() => {
		221
	};
}
#[macro_export]
macro_rules! KEY_ALTERASE {
	() => {
		222
	};
}
#[macro_export]
macro_rules! KEY_CANCEL {
	() => {
		223
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESSDOWN {
	() => {
		224
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESSUP {
	() => {
		225
	};
}
#[macro_export]
macro_rules! KEY_MEDIA {
	() => {
		226
	};
}

#[macro_export]
macro_rules! KEY_SWITCHVIDEOMODE {
	() => {
		227
	};
}
#[macro_export]
macro_rules! KEY_KBDILLUMTOGGLE {
	() => {
		228
	};
}
#[macro_export]
macro_rules! KEY_KBDILLUMDOWN {
	() => {
		229
	};
}
#[macro_export]
macro_rules! KEY_KBDILLUMUP {
	() => {
		230
	};
}

#[macro_export]
macro_rules! KEY_SEND {
	() => {
		231
	};
}
#[macro_export]
macro_rules! KEY_REPLY {
	() => {
		232
	};
}
#[macro_export]
macro_rules! KEY_FORWARDMAIL {
	() => {
		233
	};
}
#[macro_export]
macro_rules! KEY_SAVE {
	() => {
		234
	};
}
#[macro_export]
macro_rules! KEY_DOCUMENTS {
	() => {
		235
	};
}

#[macro_export]
macro_rules! KEY_BATTERY {
	() => {
		236
	};
}

#[macro_export]
macro_rules! KEY_BLUETOOTH {
	() => {
		237
	};
}
#[macro_export]
macro_rules! KEY_WLAN {
	() => {
		238
	};
}
#[macro_export]
macro_rules! KEY_UWB {
	() => {
		239
	};
}

#[macro_export]
macro_rules! KEY_UNKNOWN {
	() => {
		240
	};
}

#[macro_export]
macro_rules! KEY_VIDEO_NEXT {
	() => {
		241
	};
}
#[macro_export]
macro_rules! KEY_VIDEO_PREV {
	() => {
		242
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESS_CYCLE {
	() => {
		243
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESS_AUTO {
	() => {
		244
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESS_ZERO {
	() => {
		$crate::KEY_BRIGHTNESS_AUTO!()
	};
}
#[macro_export]
macro_rules! KEY_DISPLAY_OFF {
	() => {
		245
	};
}

#[macro_export]
macro_rules! KEY_WWAN {
	() => {
		246
	};
}
#[macro_export]
macro_rules! KEY_WIMAX {
	() => {
		$crate::KEY_WWAN!()
	};
}
#[macro_export]
macro_rules! KEY_RFKILL {
	() => {
		247
	};
}

#[macro_export]
macro_rules! KEY_MICMUTE {
	() => {
		248
	};
}

#[macro_export]
macro_rules! BTN_MISC {
	() => {
		0x100
	};
}
#[macro_export]
macro_rules! BTN_0 {
	() => {
		0x100
	};
}
#[macro_export]
macro_rules! BTN_1 {
	() => {
		0x101
	};
}
#[macro_export]
macro_rules! BTN_2 {
	() => {
		0x102
	};
}
#[macro_export]
macro_rules! BTN_3 {
	() => {
		0x103
	};
}
#[macro_export]
macro_rules! BTN_4 {
	() => {
		0x104
	};
}
#[macro_export]
macro_rules! BTN_5 {
	() => {
		0x105
	};
}
#[macro_export]
macro_rules! BTN_6 {
	() => {
		0x106
	};
}
#[macro_export]
macro_rules! BTN_7 {
	() => {
		0x107
	};
}
#[macro_export]
macro_rules! BTN_8 {
	() => {
		0x108
	};
}
#[macro_export]
macro_rules! BTN_9 {
	() => {
		0x109
	};
}

#[macro_export]
macro_rules! BTN_MOUSE {
	() => {
		0x110
	};
}
#[macro_export]
macro_rules! BTN_LEFT {
	() => {
		0x110
	};
}
#[macro_export]
macro_rules! BTN_RIGHT {
	() => {
		0x111
	};
}
#[macro_export]
macro_rules! BTN_MIDDLE {
	() => {
		0x112
	};
}
#[macro_export]
macro_rules! BTN_SIDE {
	() => {
		0x113
	};
}
#[macro_export]
macro_rules! BTN_EXTRA {
	() => {
		0x114
	};
}
#[macro_export]
macro_rules! BTN_FORWARD {
	() => {
		0x115
	};
}
#[macro_export]
macro_rules! BTN_BACK {
	() => {
		0x116
	};
}
#[macro_export]
macro_rules! BTN_TASK {
	() => {
		0x117
	};
}

#[macro_export]
macro_rules! BTN_JOYSTICK {
	() => {
		0x120
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER {
	() => {
		0x120
	};
}
#[macro_export]
macro_rules! BTN_THUMB {
	() => {
		0x121
	};
}
#[macro_export]
macro_rules! BTN_THUMB2 {
	() => {
		0x122
	};
}
#[macro_export]
macro_rules! BTN_TOP {
	() => {
		0x123
	};
}
#[macro_export]
macro_rules! BTN_TOP2 {
	() => {
		0x124
	};
}
#[macro_export]
macro_rules! BTN_PINKIE {
	() => {
		0x125
	};
}
#[macro_export]
macro_rules! BTN_BASE {
	() => {
		0x126
	};
}
#[macro_export]
macro_rules! BTN_BASE2 {
	() => {
		0x127
	};
}
#[macro_export]
macro_rules! BTN_BASE3 {
	() => {
		0x128
	};
}
#[macro_export]
macro_rules! BTN_BASE4 {
	() => {
		0x129
	};
}
#[macro_export]
macro_rules! BTN_BASE5 {
	() => {
		0x12a
	};
}
#[macro_export]
macro_rules! BTN_BASE6 {
	() => {
		0x12b
	};
}
#[macro_export]
macro_rules! BTN_DEAD {
	() => {
		0x12f
	};
}

#[macro_export]
macro_rules! BTN_GAMEPAD {
	() => {
		0x130
	};
}
#[macro_export]
macro_rules! BTN_SOUTH {
	() => {
		0x130
	};
}
#[macro_export]
macro_rules! BTN_A {
	() => {
		$crate::BTN_SOUTH!()
	};
}
#[macro_export]
macro_rules! BTN_EAST {
	() => {
		0x131
	};
}
#[macro_export]
macro_rules! BTN_B {
	() => {
		$crate::BTN_EAST!()
	};
}
#[macro_export]
macro_rules! BTN_C {
	() => {
		0x132
	};
}
#[macro_export]
macro_rules! BTN_NORTH {
	() => {
		0x133
	};
}
#[macro_export]
macro_rules! BTN_X {
	() => {
		$crate::BTN_NORTH!()
	};
}
#[macro_export]
macro_rules! BTN_WEST {
	() => {
		0x134
	};
}
#[macro_export]
macro_rules! BTN_Y {
	() => {
		$crate::BTN_WEST!()
	};
}
#[macro_export]
macro_rules! BTN_Z {
	() => {
		0x135
	};
}
#[macro_export]
macro_rules! BTN_TL {
	() => {
		0x136
	};
}
#[macro_export]
macro_rules! BTN_TR {
	() => {
		0x137
	};
}
#[macro_export]
macro_rules! BTN_TL2 {
	() => {
		0x138
	};
}
#[macro_export]
macro_rules! BTN_TR2 {
	() => {
		0x139
	};
}
#[macro_export]
macro_rules! BTN_SELECT {
	() => {
		0x13a
	};
}
#[macro_export]
macro_rules! BTN_START {
	() => {
		0x13b
	};
}
#[macro_export]
macro_rules! BTN_MODE {
	() => {
		0x13c
	};
}
#[macro_export]
macro_rules! BTN_THUMBL {
	() => {
		0x13d
	};
}
#[macro_export]
macro_rules! BTN_THUMBR {
	() => {
		0x13e
	};
}

#[macro_export]
macro_rules! BTN_DIGI {
	() => {
		0x140
	};
}
#[macro_export]
macro_rules! BTN_TOOL_PEN {
	() => {
		0x140
	};
}
#[macro_export]
macro_rules! BTN_TOOL_RUBBER {
	() => {
		0x141
	};
}
#[macro_export]
macro_rules! BTN_TOOL_BRUSH {
	() => {
		0x142
	};
}
#[macro_export]
macro_rules! BTN_TOOL_PENCIL {
	() => {
		0x143
	};
}
#[macro_export]
macro_rules! BTN_TOOL_AIRBRUSH {
	() => {
		0x144
	};
}
#[macro_export]
macro_rules! BTN_TOOL_FINGER {
	() => {
		0x145
	};
}
#[macro_export]
macro_rules! BTN_TOOL_MOUSE {
	() => {
		0x146
	};
}
#[macro_export]
macro_rules! BTN_TOOL_LENS {
	() => {
		0x147
	};
}
#[macro_export]
macro_rules! BTN_TOOL_QUINTTAP {
	() => {
		0x148
	};
}
#[macro_export]
macro_rules! BTN_STYLUS3 {
	() => {
		0x149
	};
}
#[macro_export]
macro_rules! BTN_TOUCH {
	() => {
		0x14a
	};
}
#[macro_export]
macro_rules! BTN_STYLUS {
	() => {
		0x14b
	};
}
#[macro_export]
macro_rules! BTN_STYLUS2 {
	() => {
		0x14c
	};
}
#[macro_export]
macro_rules! BTN_TOOL_DOUBLETAP {
	() => {
		0x14d
	};
}
#[macro_export]
macro_rules! BTN_TOOL_TRIPLETAP {
	() => {
		0x14e
	};
}
#[macro_export]
macro_rules! BTN_TOOL_QUADTAP {
	() => {
		0x14f
	};
}

#[macro_export]
macro_rules! BTN_WHEEL {
	() => {
		0x150
	};
}
#[macro_export]
macro_rules! BTN_GEAR_DOWN {
	() => {
		0x150
	};
}
#[macro_export]
macro_rules! BTN_GEAR_UP {
	() => {
		0x151
	};
}

#[macro_export]
macro_rules! KEY_OK {
	() => {
		0x160
	};
}
#[macro_export]
macro_rules! KEY_SELECT {
	() => {
		0x161
	};
}
#[macro_export]
macro_rules! KEY_GOTO {
	() => {
		0x162
	};
}
#[macro_export]
macro_rules! KEY_CLEAR {
	() => {
		0x163
	};
}
#[macro_export]
macro_rules! KEY_POWER2 {
	() => {
		0x164
	};
}
#[macro_export]
macro_rules! KEY_OPTION {
	() => {
		0x165
	};
}
#[macro_export]
macro_rules! KEY_INFO {
	() => {
		0x166
	};
}
#[macro_export]
macro_rules! KEY_TIME {
	() => {
		0x167
	};
}
#[macro_export]
macro_rules! KEY_VENDOR {
	() => {
		0x168
	};
}
#[macro_export]
macro_rules! KEY_ARCHIVE {
	() => {
		0x169
	};
}
#[macro_export]
macro_rules! KEY_PROGRAM {
	() => {
		0x16a
	};
}
#[macro_export]
macro_rules! KEY_CHANNEL {
	() => {
		0x16b
	};
}
#[macro_export]
macro_rules! KEY_FAVORITES {
	() => {
		0x16c
	};
}
#[macro_export]
macro_rules! KEY_EPG {
	() => {
		0x16d
	};
}
#[macro_export]
macro_rules! KEY_PVR {
	() => {
		0x16e
	};
}
#[macro_export]
macro_rules! KEY_MHP {
	() => {
		0x16f
	};
}
#[macro_export]
macro_rules! KEY_LANGUAGE {
	() => {
		0x170
	};
}
#[macro_export]
macro_rules! KEY_TITLE {
	() => {
		0x171
	};
}
#[macro_export]
macro_rules! KEY_SUBTITLE {
	() => {
		0x172
	};
}
#[macro_export]
macro_rules! KEY_ANGLE {
	() => {
		0x173
	};
}
#[macro_export]
macro_rules! KEY_FULL_SCREEN {
	() => {
		0x174
	};
}
#[macro_export]
macro_rules! KEY_ZOOM {
	() => {
		$crate::KEY_FULL_SCREEN!()
	};
}
#[macro_export]
macro_rules! KEY_MODE {
	() => {
		0x175
	};
}
#[macro_export]
macro_rules! KEY_KEYBOARD {
	() => {
		0x176
	};
}
#[macro_export]
macro_rules! KEY_ASPECT_RATIO {
	() => {
		0x177
	};
}
#[macro_export]
macro_rules! KEY_SCREEN {
	() => {
		$crate::KEY_ASPECT_RATIO!()
	};
}
#[macro_export]
macro_rules! KEY_PC {
	() => {
		0x178
	};
}
#[macro_export]
macro_rules! KEY_TV {
	() => {
		0x179
	};
}
#[macro_export]
macro_rules! KEY_TV2 {
	() => {
		0x17a
	};
}
#[macro_export]
macro_rules! KEY_VCR {
	() => {
		0x17b
	};
}
#[macro_export]
macro_rules! KEY_VCR2 {
	() => {
		0x17c
	};
}
#[macro_export]
macro_rules! KEY_SAT {
	() => {
		0x17d
	};
}
#[macro_export]
macro_rules! KEY_SAT2 {
	() => {
		0x17e
	};
}
#[macro_export]
macro_rules! KEY_CD {
	() => {
		0x17f
	};
}
#[macro_export]
macro_rules! KEY_TAPE {
	() => {
		0x180
	};
}
#[macro_export]
macro_rules! KEY_RADIO {
	() => {
		0x181
	};
}
#[macro_export]
macro_rules! KEY_TUNER {
	() => {
		0x182
	};
}
#[macro_export]
macro_rules! KEY_PLAYER {
	() => {
		0x183
	};
}
#[macro_export]
macro_rules! KEY_TEXT {
	() => {
		0x184
	};
}
#[macro_export]
macro_rules! KEY_DVD {
	() => {
		0x185
	};
}
#[macro_export]
macro_rules! KEY_AUX {
	() => {
		0x186
	};
}
#[macro_export]
macro_rules! KEY_MP3 {
	() => {
		0x187
	};
}
#[macro_export]
macro_rules! KEY_AUDIO {
	() => {
		0x188
	};
}
#[macro_export]
macro_rules! KEY_VIDEO {
	() => {
		0x189
	};
}
#[macro_export]
macro_rules! KEY_DIRECTORY {
	() => {
		0x18a
	};
}
#[macro_export]
macro_rules! KEY_LIST {
	() => {
		0x18b
	};
}
#[macro_export]
macro_rules! KEY_MEMO {
	() => {
		0x18c
	};
}
#[macro_export]
macro_rules! KEY_CALENDAR {
	() => {
		0x18d
	};
}
#[macro_export]
macro_rules! KEY_RED {
	() => {
		0x18e
	};
}
#[macro_export]
macro_rules! KEY_GREEN {
	() => {
		0x18f
	};
}
#[macro_export]
macro_rules! KEY_YELLOW {
	() => {
		0x190
	};
}
#[macro_export]
macro_rules! KEY_BLUE {
	() => {
		0x191
	};
}
#[macro_export]
macro_rules! KEY_CHANNELUP {
	() => {
		0x192
	};
}
#[macro_export]
macro_rules! KEY_CHANNELDOWN {
	() => {
		0x193
	};
}
#[macro_export]
macro_rules! KEY_FIRST {
	() => {
		0x194
	};
}
#[macro_export]
macro_rules! KEY_LAST {
	() => {
		0x195
	};
}
#[macro_export]
macro_rules! KEY_AB {
	() => {
		0x196
	};
}
#[macro_export]
macro_rules! KEY_NEXT {
	() => {
		0x197
	};
}
#[macro_export]
macro_rules! KEY_RESTART {
	() => {
		0x198
	};
}
#[macro_export]
macro_rules! KEY_SLOW {
	() => {
		0x199
	};
}
#[macro_export]
macro_rules! KEY_SHUFFLE {
	() => {
		0x19a
	};
}
#[macro_export]
macro_rules! KEY_BREAK {
	() => {
		0x19b
	};
}
#[macro_export]
macro_rules! KEY_PREVIOUS {
	() => {
		0x19c
	};
}
#[macro_export]
macro_rules! KEY_DIGITS {
	() => {
		0x19d
	};
}
#[macro_export]
macro_rules! KEY_TEEN {
	() => {
		0x19e
	};
}
#[macro_export]
macro_rules! KEY_TWEN {
	() => {
		0x19f
	};
}
#[macro_export]
macro_rules! KEY_VIDEOPHONE {
	() => {
		0x1a0
	};
}
#[macro_export]
macro_rules! KEY_GAMES {
	() => {
		0x1a1
	};
}
#[macro_export]
macro_rules! KEY_ZOOMIN {
	() => {
		0x1a2
	};
}
#[macro_export]
macro_rules! KEY_ZOOMOUT {
	() => {
		0x1a3
	};
}
#[macro_export]
macro_rules! KEY_ZOOMRESET {
	() => {
		0x1a4
	};
}
#[macro_export]
macro_rules! KEY_WORDPROCESSOR {
	() => {
		0x1a5
	};
}
#[macro_export]
macro_rules! KEY_EDITOR {
	() => {
		0x1a6
	};
}
#[macro_export]
macro_rules! KEY_SPREADSHEET {
	() => {
		0x1a7
	};
}
#[macro_export]
macro_rules! KEY_GRAPHICSEDITOR {
	() => {
		0x1a8
	};
}
#[macro_export]
macro_rules! KEY_PRESENTATION {
	() => {
		0x1a9
	};
}
#[macro_export]
macro_rules! KEY_DATABASE {
	() => {
		0x1aa
	};
}
#[macro_export]
macro_rules! KEY_NEWS {
	() => {
		0x1ab
	};
}
#[macro_export]
macro_rules! KEY_VOICEMAIL {
	() => {
		0x1ac
	};
}
#[macro_export]
macro_rules! KEY_ADDRESSBOOK {
	() => {
		0x1ad
	};
}
#[macro_export]
macro_rules! KEY_MESSENGER {
	() => {
		0x1ae
	};
}
#[macro_export]
macro_rules! KEY_DISPLAYTOGGLE {
	() => {
		0x1af
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESS_TOGGLE {
	() => {
		$crate::KEY_DISPLAYTOGGLE!()
	};
}
#[macro_export]
macro_rules! KEY_SPELLCHECK {
	() => {
		0x1b0
	};
}
#[macro_export]
macro_rules! KEY_LOGOFF {
	() => {
		0x1b1
	};
}

#[macro_export]
macro_rules! KEY_DOLLAR {
	() => {
		0x1b2
	};
}
#[macro_export]
macro_rules! KEY_EURO {
	() => {
		0x1b3
	};
}

#[macro_export]
macro_rules! KEY_FRAMEBACK {
	() => {
		0x1b4
	};
}
#[macro_export]
macro_rules! KEY_FRAMEFORWARD {
	() => {
		0x1b5
	};
}
#[macro_export]
macro_rules! KEY_CONTEXT_MENU {
	() => {
		0x1b6
	};
}
#[macro_export]
macro_rules! KEY_MEDIA_REPEAT {
	() => {
		0x1b7
	};
}
#[macro_export]
macro_rules! KEY_10CHANNELSUP {
	() => {
		0x1b8
	};
}
#[macro_export]
macro_rules! KEY_10CHANNELSDOWN {
	() => {
		0x1b9
	};
}
#[macro_export]
macro_rules! KEY_IMAGES {
	() => {
		0x1ba
	};
}
#[macro_export]
macro_rules! KEY_NOTIFICATION_CENTER {
	() => {
		0x1bc
	};
}
#[macro_export]
macro_rules! KEY_PICKUP_PHONE {
	() => {
		0x1bd
	};
}
#[macro_export]
macro_rules! KEY_HANGUP_PHONE {
	() => {
		0x1be
	};
}

#[macro_export]
macro_rules! KEY_DEL_EOL {
	() => {
		0x1c0
	};
}
#[macro_export]
macro_rules! KEY_DEL_EOS {
	() => {
		0x1c1
	};
}
#[macro_export]
macro_rules! KEY_INS_LINE {
	() => {
		0x1c2
	};
}
#[macro_export]
macro_rules! KEY_DEL_LINE {
	() => {
		0x1c3
	};
}

#[macro_export]
macro_rules! KEY_FN {
	() => {
		0x1d0
	};
}
#[macro_export]
macro_rules! KEY_FN_ESC {
	() => {
		0x1d1
	};
}
#[macro_export]
macro_rules! KEY_FN_F1 {
	() => {
		0x1d2
	};
}
#[macro_export]
macro_rules! KEY_FN_F2 {
	() => {
		0x1d3
	};
}
#[macro_export]
macro_rules! KEY_FN_F3 {
	() => {
		0x1d4
	};
}
#[macro_export]
macro_rules! KEY_FN_F4 {
	() => {
		0x1d5
	};
}
#[macro_export]
macro_rules! KEY_FN_F5 {
	() => {
		0x1d6
	};
}
#[macro_export]
macro_rules! KEY_FN_F6 {
	() => {
		0x1d7
	};
}
#[macro_export]
macro_rules! KEY_FN_F7 {
	() => {
		0x1d8
	};
}
#[macro_export]
macro_rules! KEY_FN_F8 {
	() => {
		0x1d9
	};
}
#[macro_export]
macro_rules! KEY_FN_F9 {
	() => {
		0x1da
	};
}
#[macro_export]
macro_rules! KEY_FN_F10 {
	() => {
		0x1db
	};
}
#[macro_export]
macro_rules! KEY_FN_F11 {
	() => {
		0x1dc
	};
}
#[macro_export]
macro_rules! KEY_FN_F12 {
	() => {
		0x1dd
	};
}
#[macro_export]
macro_rules! KEY_FN_1 {
	() => {
		0x1de
	};
}
#[macro_export]
macro_rules! KEY_FN_2 {
	() => {
		0x1df
	};
}
#[macro_export]
macro_rules! KEY_FN_D {
	() => {
		0x1e0
	};
}
#[macro_export]
macro_rules! KEY_FN_E {
	() => {
		0x1e1
	};
}
#[macro_export]
macro_rules! KEY_FN_F {
	() => {
		0x1e2
	};
}
#[macro_export]
macro_rules! KEY_FN_S {
	() => {
		0x1e3
	};
}
#[macro_export]
macro_rules! KEY_FN_B {
	() => {
		0x1e4
	};
}
#[macro_export]
macro_rules! KEY_FN_RIGHT_SHIFT {
	() => {
		0x1e5
	};
}

#[macro_export]
macro_rules! KEY_BRL_DOT1 {
	() => {
		0x1f1
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT2 {
	() => {
		0x1f2
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT3 {
	() => {
		0x1f3
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT4 {
	() => {
		0x1f4
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT5 {
	() => {
		0x1f5
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT6 {
	() => {
		0x1f6
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT7 {
	() => {
		0x1f7
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT8 {
	() => {
		0x1f8
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT9 {
	() => {
		0x1f9
	};
}
#[macro_export]
macro_rules! KEY_BRL_DOT10 {
	() => {
		0x1fa
	};
}

#[macro_export]
macro_rules! KEY_NUMERIC_0 {
	() => {
		0x200
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_1 {
	() => {
		0x201
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_2 {
	() => {
		0x202
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_3 {
	() => {
		0x203
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_4 {
	() => {
		0x204
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_5 {
	() => {
		0x205
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_6 {
	() => {
		0x206
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_7 {
	() => {
		0x207
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_8 {
	() => {
		0x208
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_9 {
	() => {
		0x209
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_STAR {
	() => {
		0x20a
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_POUND {
	() => {
		0x20b
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_A {
	() => {
		0x20c
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_B {
	() => {
		0x20d
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_C {
	() => {
		0x20e
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_D {
	() => {
		0x20f
	};
}

#[macro_export]
macro_rules! KEY_CAMERA_FOCUS {
	() => {
		0x210
	};
}
#[macro_export]
macro_rules! KEY_WPS_BUTTON {
	() => {
		0x211
	};
}

#[macro_export]
macro_rules! KEY_TOUCHPAD_TOGGLE {
	() => {
		0x212
	};
}
#[macro_export]
macro_rules! KEY_TOUCHPAD_ON {
	() => {
		0x213
	};
}
#[macro_export]
macro_rules! KEY_TOUCHPAD_OFF {
	() => {
		0x214
	};
}

#[macro_export]
macro_rules! KEY_CAMERA_ZOOMIN {
	() => {
		0x215
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_ZOOMOUT {
	() => {
		0x216
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_UP {
	() => {
		0x217
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_DOWN {
	() => {
		0x218
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_LEFT {
	() => {
		0x219
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_RIGHT {
	() => {
		0x21a
	};
}

#[macro_export]
macro_rules! KEY_ATTENDANT_ON {
	() => {
		0x21b
	};
}
#[macro_export]
macro_rules! KEY_ATTENDANT_OFF {
	() => {
		0x21c
	};
}
#[macro_export]
macro_rules! KEY_ATTENDANT_TOGGLE {
	() => {
		0x21d
	};
}
#[macro_export]
macro_rules! KEY_LIGHTS_TOGGLE {
	() => {
		0x21e
	};
}

#[macro_export]
macro_rules! BTN_DPAD_UP {
	() => {
		0x220
	};
}
#[macro_export]
macro_rules! BTN_DPAD_DOWN {
	() => {
		0x221
	};
}
#[macro_export]
macro_rules! BTN_DPAD_LEFT {
	() => {
		0x222
	};
}
#[macro_export]
macro_rules! BTN_DPAD_RIGHT {
	() => {
		0x223
	};
}

#[macro_export]
macro_rules! KEY_ALS_TOGGLE {
	() => {
		0x230
	};
}
#[macro_export]
macro_rules! KEY_ROTATE_LOCK_TOGGLE {
	() => {
		0x231
	};
}

#[macro_export]
macro_rules! KEY_BUTTONCONFIG {
	() => {
		0x240
	};
}
#[macro_export]
macro_rules! KEY_TASKMANAGER {
	() => {
		0x241
	};
}
#[macro_export]
macro_rules! KEY_JOURNAL {
	() => {
		0x242
	};
}
#[macro_export]
macro_rules! KEY_CONTROLPANEL {
	() => {
		0x243
	};
}
#[macro_export]
macro_rules! KEY_APPSELECT {
	() => {
		0x244
	};
}
#[macro_export]
macro_rules! KEY_SCREENSAVER {
	() => {
		0x245
	};
}
#[macro_export]
macro_rules! KEY_VOICECOMMAND {
	() => {
		0x246
	};
}
#[macro_export]
macro_rules! KEY_ASSISTANT {
	() => {
		0x247
	};
}
#[macro_export]
macro_rules! KEY_KBD_LAYOUT_NEXT {
	() => {
		0x248
	};
}
#[macro_export]
macro_rules! KEY_EMOJI_PICKER {
	() => {
		0x249
	};
}
#[macro_export]
macro_rules! KEY_DICTATE {
	() => {
		0x24a
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_ACCESS_ENABLE {
	() => {
		0x24b
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_ACCESS_DISABLE {
	() => {
		0x24c
	};
}
#[macro_export]
macro_rules! KEY_CAMERA_ACCESS_TOGGLE {
	() => {
		0x24d
	};
}

#[macro_export]
macro_rules! KEY_BRIGHTNESS_MIN {
	() => {
		0x250
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESS_MAX {
	() => {
		0x251
	};
}

#[macro_export]
macro_rules! KEY_KBDINPUTASSIST_PREV {
	() => {
		0x260
	};
}
#[macro_export]
macro_rules! KEY_KBDINPUTASSIST_NEXT {
	() => {
		0x261
	};
}
#[macro_export]
macro_rules! KEY_KBDINPUTASSIST_PREVGROUP {
	() => {
		0x262
	};
}
#[macro_export]
macro_rules! KEY_KBDINPUTASSIST_NEXTGROUP {
	() => {
		0x263
	};
}
#[macro_export]
macro_rules! KEY_KBDINPUTASSIST_ACCEPT {
	() => {
		0x264
	};
}
#[macro_export]
macro_rules! KEY_KBDINPUTASSIST_CANCEL {
	() => {
		0x265
	};
}

#[macro_export]
macro_rules! KEY_RIGHT_UP {
	() => {
		0x266
	};
}
#[macro_export]
macro_rules! KEY_RIGHT_DOWN {
	() => {
		0x267
	};
}
#[macro_export]
macro_rules! KEY_LEFT_UP {
	() => {
		0x268
	};
}
#[macro_export]
macro_rules! KEY_LEFT_DOWN {
	() => {
		0x269
	};
}

#[macro_export]
macro_rules! KEY_ROOT_MENU {
	() => {
		0x26a
	};
}
#[macro_export]
macro_rules! KEY_MEDIA_TOP_MENU {
	() => {
		0x26b
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_11 {
	() => {
		0x26c
	};
}
#[macro_export]
macro_rules! KEY_NUMERIC_12 {
	() => {
		0x26d
	};
}
#[macro_export]
macro_rules! KEY_AUDIO_DESC {
	() => {
		0x26e
	};
}
#[macro_export]
macro_rules! KEY_3D_MODE {
	() => {
		0x26f
	};
}
#[macro_export]
macro_rules! KEY_NEXT_FAVORITE {
	() => {
		0x270
	};
}
#[macro_export]
macro_rules! KEY_STOP_RECORD {
	() => {
		0x271
	};
}
#[macro_export]
macro_rules! KEY_PAUSE_RECORD {
	() => {
		0x272
	};
}
#[macro_export]
macro_rules! KEY_VOD {
	() => {
		0x273
	};
}
#[macro_export]
macro_rules! KEY_UNMUTE {
	() => {
		0x274
	};
}
#[macro_export]
macro_rules! KEY_FASTREVERSE {
	() => {
		0x275
	};
}
#[macro_export]
macro_rules! KEY_SLOWREVERSE {
	() => {
		0x276
	};
}
#[macro_export]
macro_rules! KEY_DATA {
	() => {
		0x277
	};
}
#[macro_export]
macro_rules! KEY_ONSCREEN_KEYBOARD {
	() => {
		0x278
	};
}
#[macro_export]
macro_rules! KEY_PRIVACY_SCREEN_TOGGLE {
	() => {
		0x279
	};
}

#[macro_export]
macro_rules! KEY_SELECTIVE_SCREENSHOT {
	() => {
		0x27a
	};
}

#[macro_export]
macro_rules! KEY_NEXT_ELEMENT {
	() => {
		0x27b
	};
}
#[macro_export]
macro_rules! KEY_PREVIOUS_ELEMENT {
	() => {
		0x27c
	};
}

#[macro_export]
macro_rules! KEY_AUTOPILOT_ENGAGE_TOGGLE {
	() => {
		0x27d
	};
}

#[macro_export]
macro_rules! KEY_MARK_WAYPOINT {
	() => {
		0x27e
	};
}
#[macro_export]
macro_rules! KEY_SOS {
	() => {
		0x27f
	};
}
#[macro_export]
macro_rules! KEY_NAV_CHART {
	() => {
		0x280
	};
}
#[macro_export]
macro_rules! KEY_FISHING_CHART {
	() => {
		0x281
	};
}
#[macro_export]
macro_rules! KEY_SINGLE_RANGE_RADAR {
	() => {
		0x282
	};
}
#[macro_export]
macro_rules! KEY_DUAL_RANGE_RADAR {
	() => {
		0x283
	};
}
#[macro_export]
macro_rules! KEY_RADAR_OVERLAY {
	() => {
		0x284
	};
}
#[macro_export]
macro_rules! KEY_TRADITIONAL_SONAR {
	() => {
		0x285
	};
}
#[macro_export]
macro_rules! KEY_CLEARVU_SONAR {
	() => {
		0x286
	};
}
#[macro_export]
macro_rules! KEY_SIDEVU_SONAR {
	() => {
		0x287
	};
}
#[macro_export]
macro_rules! KEY_NAV_INFO {
	() => {
		0x288
	};
}
#[macro_export]
macro_rules! KEY_BRIGHTNESS_MENU {
	() => {
		0x289
	};
}

#[macro_export]
macro_rules! KEY_MACRO1 {
	() => {
		0x290
	};
}
#[macro_export]
macro_rules! KEY_MACRO2 {
	() => {
		0x291
	};
}
#[macro_export]
macro_rules! KEY_MACRO3 {
	() => {
		0x292
	};
}
#[macro_export]
macro_rules! KEY_MACRO4 {
	() => {
		0x293
	};
}
#[macro_export]
macro_rules! KEY_MACRO5 {
	() => {
		0x294
	};
}
#[macro_export]
macro_rules! KEY_MACRO6 {
	() => {
		0x295
	};
}
#[macro_export]
macro_rules! KEY_MACRO7 {
	() => {
		0x296
	};
}
#[macro_export]
macro_rules! KEY_MACRO8 {
	() => {
		0x297
	};
}
#[macro_export]
macro_rules! KEY_MACRO9 {
	() => {
		0x298
	};
}
#[macro_export]
macro_rules! KEY_MACRO10 {
	() => {
		0x299
	};
}
#[macro_export]
macro_rules! KEY_MACRO11 {
	() => {
		0x29a
	};
}
#[macro_export]
macro_rules! KEY_MACRO12 {
	() => {
		0x29b
	};
}
#[macro_export]
macro_rules! KEY_MACRO13 {
	() => {
		0x29c
	};
}
#[macro_export]
macro_rules! KEY_MACRO14 {
	() => {
		0x29d
	};
}
#[macro_export]
macro_rules! KEY_MACRO15 {
	() => {
		0x29e
	};
}
#[macro_export]
macro_rules! KEY_MACRO16 {
	() => {
		0x29f
	};
}
#[macro_export]
macro_rules! KEY_MACRO17 {
	() => {
		0x2a0
	};
}
#[macro_export]
macro_rules! KEY_MACRO18 {
	() => {
		0x2a1
	};
}
#[macro_export]
macro_rules! KEY_MACRO19 {
	() => {
		0x2a2
	};
}
#[macro_export]
macro_rules! KEY_MACRO20 {
	() => {
		0x2a3
	};
}
#[macro_export]
macro_rules! KEY_MACRO21 {
	() => {
		0x2a4
	};
}
#[macro_export]
macro_rules! KEY_MACRO22 {
	() => {
		0x2a5
	};
}
#[macro_export]
macro_rules! KEY_MACRO23 {
	() => {
		0x2a6
	};
}
#[macro_export]
macro_rules! KEY_MACRO24 {
	() => {
		0x2a7
	};
}
#[macro_export]
macro_rules! KEY_MACRO25 {
	() => {
		0x2a8
	};
}
#[macro_export]
macro_rules! KEY_MACRO26 {
	() => {
		0x2a9
	};
}
#[macro_export]
macro_rules! KEY_MACRO27 {
	() => {
		0x2aa
	};
}
#[macro_export]
macro_rules! KEY_MACRO28 {
	() => {
		0x2ab
	};
}
#[macro_export]
macro_rules! KEY_MACRO29 {
	() => {
		0x2ac
	};
}
#[macro_export]
macro_rules! KEY_MACRO30 {
	() => {
		0x2ad
	};
}

#[macro_export]
macro_rules! KEY_MACRO_RECORD_START {
	() => {
		0x2b0
	};
}
#[macro_export]
macro_rules! KEY_MACRO_RECORD_STOP {
	() => {
		0x2b1
	};
}
#[macro_export]
macro_rules! KEY_MACRO_PRESET_CYCLE {
	() => {
		0x2b2
	};
}
#[macro_export]
macro_rules! KEY_MACRO_PRESET1 {
	() => {
		0x2b3
	};
}
#[macro_export]
macro_rules! KEY_MACRO_PRESET2 {
	() => {
		0x2b4
	};
}
#[macro_export]
macro_rules! KEY_MACRO_PRESET3 {
	() => {
		0x2b5
	};
}

#[macro_export]
macro_rules! KEY_KBD_LCD_MENU1 {
	() => {
		0x2b8
	};
}
#[macro_export]
macro_rules! KEY_KBD_LCD_MENU2 {
	() => {
		0x2b9
	};
}
#[macro_export]
macro_rules! KEY_KBD_LCD_MENU3 {
	() => {
		0x2ba
	};
}
#[macro_export]
macro_rules! KEY_KBD_LCD_MENU4 {
	() => {
		0x2bb
	};
}
#[macro_export]
macro_rules! KEY_KBD_LCD_MENU5 {
	() => {
		0x2bc
	};
}

#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY {
	() => {
		0x2c0
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY1 {
	() => {
		0x2c0
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY2 {
	() => {
		0x2c1
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY3 {
	() => {
		0x2c2
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY4 {
	() => {
		0x2c3
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY5 {
	() => {
		0x2c4
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY6 {
	() => {
		0x2c5
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY7 {
	() => {
		0x2c6
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY8 {
	() => {
		0x2c7
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY9 {
	() => {
		0x2c8
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY10 {
	() => {
		0x2c9
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY11 {
	() => {
		0x2ca
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY12 {
	() => {
		0x2cb
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY13 {
	() => {
		0x2cc
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY14 {
	() => {
		0x2cd
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY15 {
	() => {
		0x2ce
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY16 {
	() => {
		0x2cf
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY17 {
	() => {
		0x2d0
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY18 {
	() => {
		0x2d1
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY19 {
	() => {
		0x2d2
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY20 {
	() => {
		0x2d3
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY21 {
	() => {
		0x2d4
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY22 {
	() => {
		0x2d5
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY23 {
	() => {
		0x2d6
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY24 {
	() => {
		0x2d7
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY25 {
	() => {
		0x2d8
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY26 {
	() => {
		0x2d9
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY27 {
	() => {
		0x2da
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY28 {
	() => {
		0x2db
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY29 {
	() => {
		0x2dc
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY30 {
	() => {
		0x2dd
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY31 {
	() => {
		0x2de
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY32 {
	() => {
		0x2df
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY33 {
	() => {
		0x2e0
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY34 {
	() => {
		0x2e1
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY35 {
	() => {
		0x2e2
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY36 {
	() => {
		0x2e3
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY37 {
	() => {
		0x2e4
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY38 {
	() => {
		0x2e5
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY39 {
	() => {
		0x2e6
	};
}
#[macro_export]
macro_rules! BTN_TRIGGER_HAPPY40 {
	() => {
		0x2e7
	};
}

#[macro_export]
macro_rules! KEY_MIN_INTERESTING {
	() => {
		$crate::KEY_MUTE!()
	};
}
#[macro_export]
macro_rules! KEY_MAX {
	() => {
		0x2ff
	};
}
#[macro_export]
macro_rules! KEY_CNT {
	() => {
		($crate::KEY_MAX!() + 1)
	};
}
