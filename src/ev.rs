#[macro_export]
macro_rules! EV_SYN {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! EV_KEY {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! EV_REL {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! EV_ABS {
	() => {
		0x03
	};
}
#[macro_export]
macro_rules! EV_MSC {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! EV_SW {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! EV_LED {
	() => {
		0x11
	};
}
#[macro_export]
macro_rules! EV_SND {
	() => {
		0x12
	};
}
#[macro_export]
macro_rules! EV_REP {
	() => {
		0x14
	};
}
#[macro_export]
macro_rules! EV_FF {
	() => {
		0x15
	};
}
#[macro_export]
macro_rules! EV_PWR {
	() => {
		0x16
	};
}
#[macro_export]
macro_rules! EV_FF_STATUS {
	() => {
		0x17
	};
}
#[macro_export]
macro_rules! EV_MAX {
	() => {
		0x1f
	};
}
#[macro_export]
macro_rules! EV_CNT {
	() => {
		($crate::EV_MAX!() + 1)
	};
}
