#[macro_export]
macro_rules! SW_LID {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! SW_TABLET_MODE {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! SW_HEADPHONE_INSERT {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! SW_RFKILL_ALL {
	() => {
		0x03
	};
}
// Deprecated since v2.6.26-rc5, kernel commit hash 5adad0133907790c50283bf03271d920d6897043
#[deprecated(since = "2.6.26", note = "Use SW_RFKILL_ALL instead")]
#[macro_export]
macro_rules! SW_RADIO {
	() => {
		$crate::SW_RFKILL_ALL!()
	};
}
#[macro_export]
macro_rules! SW_MICROPHONE_INSERT {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! SW_DOCK {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! SW_LINEOUT_INSERT {
	() => {
		0x06
	};
}
#[macro_export]
macro_rules! SW_JACK_PHYSICAL_INSERT {
	() => {
		0x07
	};
}
#[macro_export]
macro_rules! SW_VIDEOOUT_INSERT {
	() => {
		0x08
	};
}
#[macro_export]
macro_rules! SW_CAMERA_LENS_COVER {
	() => {
		0x09
	};
}
#[macro_export]
macro_rules! SW_KEYPAD_SLIDE {
	() => {
		0x0a
	};
}
#[macro_export]
macro_rules! SW_FRONT_PROXIMITY {
	() => {
		0x0b
	};
}
#[macro_export]
macro_rules! SW_ROTATE_LOCK {
	() => {
		0x0c
	};
}
#[macro_export]
macro_rules! SW_LINEIN_INSERT {
	() => {
		0x0d
	};
}
#[macro_export]
macro_rules! SW_MUTE_DEVICE {
	() => {
		0x0e
	};
}
#[macro_export]
macro_rules! SW_PEN_INSERTED {
	() => {
		0x0f
	};
}
#[macro_export]
macro_rules! SW_MACHINE_COVER {
	() => {
		0x10
	};
}
#[macro_export]
macro_rules! SW_MAX {
	() => {
		0x10
	};
}
#[macro_export]
macro_rules! SW_CNT {
	() => {
		($crate::SW_MAX!() + 1)
	};
}
