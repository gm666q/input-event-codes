#[macro_export]
macro_rules! LED_NUML {
	() => {
		0x00
	};
}
#[macro_export]
macro_rules! LED_CAPSL {
	() => {
		0x01
	};
}
#[macro_export]
macro_rules! LED_SCROLLL {
	() => {
		0x02
	};
}
#[macro_export]
macro_rules! LED_COMPOSE {
	() => {
		0x03
	};
}
#[macro_export]
macro_rules! LED_KANA {
	() => {
		0x04
	};
}
#[macro_export]
macro_rules! LED_SLEEP {
	() => {
		0x05
	};
}
#[macro_export]
macro_rules! LED_SUSPEND {
	() => {
		0x06
	};
}
#[macro_export]
macro_rules! LED_MUTE {
	() => {
		0x07
	};
}
#[macro_export]
macro_rules! LED_MISC {
	() => {
		0x08
	};
}
#[macro_export]
macro_rules! LED_MAIL {
	() => {
		0x09
	};
}
#[macro_export]
macro_rules! LED_CHARGING {
	() => {
		0x0a
	};
}
#[macro_export]
macro_rules! LED_MAX {
	() => {
		0x0f
	};
}
#[macro_export]
macro_rules! LED_CNT {
	() => {
		($crate::LED_MAX!() + 1)
	};
}
