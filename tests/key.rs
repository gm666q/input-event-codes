#![allow(non_snake_case)]

use input_event_codes::{
	BTN_0, BTN_1, BTN_2, BTN_3, BTN_4, BTN_5, BTN_6, BTN_7, BTN_8, BTN_9, BTN_A, BTN_B, BTN_BACK,
	BTN_BASE, BTN_BASE2, BTN_BASE3, BTN_BASE4, BTN_BASE5, BTN_BASE6, BTN_C, BTN_DEAD, BTN_DIGI,
	BTN_DPAD_DOWN, BTN_DPAD_LEFT, BTN_DPAD_RIGHT, BTN_DPAD_UP, BTN_EAST, BTN_EXTRA, BTN_FORWARD,
	BTN_GAMEPAD, BTN_GEAR_DOWN, BTN_GEAR_UP, BTN_JOYSTICK, BTN_LEFT, BTN_MIDDLE, BTN_MISC,
	BTN_MODE, BTN_MOUSE, BTN_NORTH, BTN_PINKIE, BTN_RIGHT, BTN_SELECT, BTN_SIDE, BTN_SOUTH,
	BTN_START, BTN_STYLUS, BTN_STYLUS2, BTN_STYLUS3, BTN_TASK, BTN_THUMB, BTN_THUMB2, BTN_THUMBL,
	BTN_THUMBR, BTN_TL, BTN_TL2, BTN_TOOL_AIRBRUSH, BTN_TOOL_BRUSH, BTN_TOOL_DOUBLETAP,
	BTN_TOOL_FINGER, BTN_TOOL_LENS, BTN_TOOL_MOUSE, BTN_TOOL_PEN, BTN_TOOL_PENCIL,
	BTN_TOOL_QUADTAP, BTN_TOOL_QUINTTAP, BTN_TOOL_RUBBER, BTN_TOOL_TRIPLETAP, BTN_TOP, BTN_TOP2,
	BTN_TOUCH, BTN_TR, BTN_TR2, BTN_TRIGGER, BTN_TRIGGER_HAPPY, BTN_TRIGGER_HAPPY1,
	BTN_TRIGGER_HAPPY10, BTN_TRIGGER_HAPPY11, BTN_TRIGGER_HAPPY12, BTN_TRIGGER_HAPPY13,
	BTN_TRIGGER_HAPPY14, BTN_TRIGGER_HAPPY15, BTN_TRIGGER_HAPPY16, BTN_TRIGGER_HAPPY17,
	BTN_TRIGGER_HAPPY18, BTN_TRIGGER_HAPPY19, BTN_TRIGGER_HAPPY2, BTN_TRIGGER_HAPPY20,
	BTN_TRIGGER_HAPPY21, BTN_TRIGGER_HAPPY22, BTN_TRIGGER_HAPPY23, BTN_TRIGGER_HAPPY24,
	BTN_TRIGGER_HAPPY25, BTN_TRIGGER_HAPPY26, BTN_TRIGGER_HAPPY27, BTN_TRIGGER_HAPPY28,
	BTN_TRIGGER_HAPPY29, BTN_TRIGGER_HAPPY3, BTN_TRIGGER_HAPPY30, BTN_TRIGGER_HAPPY31,
	BTN_TRIGGER_HAPPY32, BTN_TRIGGER_HAPPY33, BTN_TRIGGER_HAPPY34, BTN_TRIGGER_HAPPY35,
	BTN_TRIGGER_HAPPY36, BTN_TRIGGER_HAPPY37, BTN_TRIGGER_HAPPY38, BTN_TRIGGER_HAPPY39,
	BTN_TRIGGER_HAPPY4, BTN_TRIGGER_HAPPY40, BTN_TRIGGER_HAPPY5, BTN_TRIGGER_HAPPY6,
	BTN_TRIGGER_HAPPY7, BTN_TRIGGER_HAPPY8, BTN_TRIGGER_HAPPY9, BTN_WEST, BTN_WHEEL, BTN_X, BTN_Y,
	BTN_Z, KEY_0, KEY_1, KEY_102ND, KEY_10CHANNELSDOWN, KEY_10CHANNELSUP, KEY_2, KEY_3,
	KEY_3D_MODE, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_A, KEY_AB, KEY_ADDRESSBOOK,
	KEY_AGAIN, KEY_ALL_APPLICATIONS, KEY_ALS_TOGGLE, KEY_ALTERASE, KEY_ANGLE, KEY_APOSTROPHE,
	KEY_APPSELECT, KEY_ARCHIVE, KEY_ASPECT_RATIO, KEY_ASSISTANT, KEY_ATTENDANT_OFF,
	KEY_ATTENDANT_ON, KEY_ATTENDANT_TOGGLE, KEY_AUDIO, KEY_AUDIO_DESC, KEY_AUTOPILOT_ENGAGE_TOGGLE,
	KEY_AUX, KEY_B, KEY_BACK, KEY_BACKSLASH, KEY_BACKSPACE, KEY_BASSBOOST, KEY_BATTERY, KEY_BLUE,
	KEY_BLUETOOTH, KEY_BOOKMARKS, KEY_BREAK, KEY_BRIGHTNESSDOWN, KEY_BRIGHTNESSUP,
	KEY_BRIGHTNESS_AUTO, KEY_BRIGHTNESS_CYCLE, KEY_BRIGHTNESS_MAX, KEY_BRIGHTNESS_MENU,
	KEY_BRIGHTNESS_MIN, KEY_BRIGHTNESS_TOGGLE, KEY_BRIGHTNESS_ZERO, KEY_BRL_DOT1, KEY_BRL_DOT10,
	KEY_BRL_DOT2, KEY_BRL_DOT3, KEY_BRL_DOT4, KEY_BRL_DOT5, KEY_BRL_DOT6, KEY_BRL_DOT7,
	KEY_BRL_DOT8, KEY_BRL_DOT9, KEY_BUTTONCONFIG, KEY_C, KEY_CALC, KEY_CALENDAR, KEY_CAMERA,
	KEY_CAMERA_ACCESS_DISABLE, KEY_CAMERA_ACCESS_ENABLE, KEY_CAMERA_ACCESS_TOGGLE, KEY_CAMERA_DOWN,
	KEY_CAMERA_FOCUS, KEY_CAMERA_LEFT, KEY_CAMERA_RIGHT, KEY_CAMERA_UP, KEY_CAMERA_ZOOMIN,
	KEY_CAMERA_ZOOMOUT, KEY_CANCEL, KEY_CAPSLOCK, KEY_CD, KEY_CHANNEL, KEY_CHANNELDOWN,
	KEY_CHANNELUP, KEY_CHAT, KEY_CLEAR, KEY_CLEARVU_SONAR, KEY_CLOSE, KEY_CLOSECD, KEY_CNT,
	KEY_COFFEE, KEY_COMMA, KEY_COMPOSE, KEY_COMPUTER, KEY_CONFIG, KEY_CONNECT, KEY_CONTEXT_MENU,
	KEY_CONTROLPANEL, KEY_COPY, KEY_CUT, KEY_CYCLEWINDOWS, KEY_D, KEY_DASHBOARD, KEY_DATA,
	KEY_DATABASE, KEY_DELETE, KEY_DELETEFILE, KEY_DEL_EOL, KEY_DEL_EOS, KEY_DEL_LINE, KEY_DICTATE,
	KEY_DIGITS, KEY_DIRECTION, KEY_DIRECTORY, KEY_DISPLAYTOGGLE, KEY_DISPLAY_OFF, KEY_DOCUMENTS,
	KEY_DOLLAR, KEY_DOT, KEY_DOWN, KEY_DUAL_RANGE_RADAR, KEY_DVD, KEY_E, KEY_EDIT, KEY_EDITOR,
	KEY_EJECTCD, KEY_EJECTCLOSECD, KEY_EMAIL, KEY_EMOJI_PICKER, KEY_END, KEY_ENTER, KEY_EPG,
	KEY_EQUAL, KEY_ESC, KEY_EURO, KEY_EXIT, KEY_F, KEY_F1, KEY_F10, KEY_F11, KEY_F12, KEY_F13,
	KEY_F14, KEY_F15, KEY_F16, KEY_F17, KEY_F18, KEY_F19, KEY_F2, KEY_F20, KEY_F21, KEY_F22,
	KEY_F23, KEY_F24, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_FASTFORWARD,
	KEY_FASTREVERSE, KEY_FAVORITES, KEY_FILE, KEY_FINANCE, KEY_FIND, KEY_FIRST, KEY_FISHING_CHART,
	KEY_FN, KEY_FN_1, KEY_FN_2, KEY_FN_B, KEY_FN_D, KEY_FN_E, KEY_FN_ESC, KEY_FN_F, KEY_FN_F1,
	KEY_FN_F10, KEY_FN_F11, KEY_FN_F12, KEY_FN_F2, KEY_FN_F3, KEY_FN_F4, KEY_FN_F5, KEY_FN_F6,
	KEY_FN_F7, KEY_FN_F8, KEY_FN_F9, KEY_FN_RIGHT_SHIFT, KEY_FN_S, KEY_FORWARD, KEY_FORWARDMAIL,
	KEY_FRAMEBACK, KEY_FRAMEFORWARD, KEY_FRONT, KEY_FULL_SCREEN, KEY_G, KEY_GAMES, KEY_GOTO,
	KEY_GRAPHICSEDITOR, KEY_GRAVE, KEY_GREEN, KEY_H, KEY_HANGEUL, KEY_HANGUEL, KEY_HANGUP_PHONE,
	KEY_HANJA, KEY_HELP, KEY_HENKAN, KEY_HIRAGANA, KEY_HOME, KEY_HOMEPAGE, KEY_HP, KEY_I,
	KEY_IMAGES, KEY_INFO, KEY_INSERT, KEY_INS_LINE, KEY_ISO, KEY_J, KEY_JOURNAL, KEY_K,
	KEY_KATAKANA, KEY_KATAKANAHIRAGANA, KEY_KBDILLUMDOWN, KEY_KBDILLUMTOGGLE, KEY_KBDILLUMUP,
	KEY_KBDINPUTASSIST_ACCEPT, KEY_KBDINPUTASSIST_CANCEL, KEY_KBDINPUTASSIST_NEXT,
	KEY_KBDINPUTASSIST_NEXTGROUP, KEY_KBDINPUTASSIST_PREV, KEY_KBDINPUTASSIST_PREVGROUP,
	KEY_KBD_LAYOUT_NEXT, KEY_KBD_LCD_MENU1, KEY_KBD_LCD_MENU2, KEY_KBD_LCD_MENU3,
	KEY_KBD_LCD_MENU4, KEY_KBD_LCD_MENU5, KEY_KEYBOARD, KEY_KP0, KEY_KP1, KEY_KP2, KEY_KP3,
	KEY_KP4, KEY_KP5, KEY_KP6, KEY_KP7, KEY_KP8, KEY_KP9, KEY_KPASTERISK, KEY_KPCOMMA, KEY_KPDOT,
	KEY_KPENTER, KEY_KPEQUAL, KEY_KPJPCOMMA, KEY_KPLEFTPAREN, KEY_KPMINUS, KEY_KPPLUS,
	KEY_KPPLUSMINUS, KEY_KPRIGHTPAREN, KEY_KPSLASH, KEY_L, KEY_LANGUAGE, KEY_LAST, KEY_LEFT,
	KEY_LEFTALT, KEY_LEFTBRACE, KEY_LEFTCTRL, KEY_LEFTMETA, KEY_LEFTSHIFT, KEY_LEFT_DOWN,
	KEY_LEFT_UP, KEY_LIGHTS_TOGGLE, KEY_LINEFEED, KEY_LIST, KEY_LOGOFF, KEY_M, KEY_MACRO,
	KEY_MACRO1, KEY_MACRO10, KEY_MACRO11, KEY_MACRO12, KEY_MACRO13, KEY_MACRO14, KEY_MACRO15,
	KEY_MACRO16, KEY_MACRO17, KEY_MACRO18, KEY_MACRO19, KEY_MACRO2, KEY_MACRO20, KEY_MACRO21,
	KEY_MACRO22, KEY_MACRO23, KEY_MACRO24, KEY_MACRO25, KEY_MACRO26, KEY_MACRO27, KEY_MACRO28,
	KEY_MACRO29, KEY_MACRO3, KEY_MACRO30, KEY_MACRO4, KEY_MACRO5, KEY_MACRO6, KEY_MACRO7,
	KEY_MACRO8, KEY_MACRO9, KEY_MACRO_PRESET1, KEY_MACRO_PRESET2, KEY_MACRO_PRESET3,
	KEY_MACRO_PRESET_CYCLE, KEY_MACRO_RECORD_START, KEY_MACRO_RECORD_STOP, KEY_MAIL,
	KEY_MARK_WAYPOINT, KEY_MAX, KEY_MEDIA, KEY_MEDIA_REPEAT, KEY_MEDIA_TOP_MENU, KEY_MEMO,
	KEY_MENU, KEY_MESSENGER, KEY_MHP, KEY_MICMUTE, KEY_MINUS, KEY_MIN_INTERESTING, KEY_MODE,
	KEY_MOVE, KEY_MP3, KEY_MSDOS, KEY_MUHENKAN, KEY_MUTE, KEY_N, KEY_NAV_CHART, KEY_NAV_INFO,
	KEY_NEW, KEY_NEWS, KEY_NEXT, KEY_NEXTSONG, KEY_NEXT_ELEMENT, KEY_NEXT_FAVORITE,
	KEY_NOTIFICATION_CENTER, KEY_NUMERIC_0, KEY_NUMERIC_1, KEY_NUMERIC_11, KEY_NUMERIC_12,
	KEY_NUMERIC_2, KEY_NUMERIC_3, KEY_NUMERIC_4, KEY_NUMERIC_5, KEY_NUMERIC_6, KEY_NUMERIC_7,
	KEY_NUMERIC_8, KEY_NUMERIC_9, KEY_NUMERIC_A, KEY_NUMERIC_B, KEY_NUMERIC_C, KEY_NUMERIC_D,
	KEY_NUMERIC_POUND, KEY_NUMERIC_STAR, KEY_NUMLOCK, KEY_O, KEY_OK, KEY_ONSCREEN_KEYBOARD,
	KEY_OPEN, KEY_OPTION, KEY_P, KEY_PAGEDOWN, KEY_PAGEUP, KEY_PASTE, KEY_PAUSE, KEY_PAUSECD,
	KEY_PAUSE_RECORD, KEY_PC, KEY_PHONE, KEY_PICKUP_PHONE, KEY_PLAY, KEY_PLAYCD, KEY_PLAYER,
	KEY_PLAYPAUSE, KEY_POWER, KEY_POWER2, KEY_PRESENTATION, KEY_PREVIOUS, KEY_PREVIOUSSONG,
	KEY_PREVIOUS_ELEMENT, KEY_PRINT, KEY_PRIVACY_SCREEN_TOGGLE, KEY_PROG1, KEY_PROG2, KEY_PROG3,
	KEY_PROG4, KEY_PROGRAM, KEY_PROPS, KEY_PVR, KEY_Q, KEY_QUESTION, KEY_R, KEY_RADAR_OVERLAY,
	KEY_RADIO, KEY_RECORD, KEY_RED, KEY_REDO, KEY_REFRESH, KEY_REPLY, KEY_RESERVED, KEY_RESTART,
	KEY_REWIND, KEY_RFKILL, KEY_RIGHT, KEY_RIGHTALT, KEY_RIGHTBRACE, KEY_RIGHTCTRL, KEY_RIGHTMETA,
	KEY_RIGHTSHIFT, KEY_RIGHT_DOWN, KEY_RIGHT_UP, KEY_RO, KEY_ROOT_MENU, KEY_ROTATE_DISPLAY,
	KEY_ROTATE_LOCK_TOGGLE, KEY_S, KEY_SAT, KEY_SAT2, KEY_SAVE, KEY_SCALE, KEY_SCREEN,
	KEY_SCREENLOCK, KEY_SCREENSAVER, KEY_SCROLLDOWN, KEY_SCROLLLOCK, KEY_SCROLLUP, KEY_SEARCH,
	KEY_SELECT, KEY_SELECTIVE_SCREENSHOT, KEY_SEMICOLON, KEY_SEND, KEY_SENDFILE, KEY_SETUP,
	KEY_SHOP, KEY_SHUFFLE, KEY_SIDEVU_SONAR, KEY_SINGLE_RANGE_RADAR, KEY_SLASH, KEY_SLEEP,
	KEY_SLOW, KEY_SLOWREVERSE, KEY_SOS, KEY_SOUND, KEY_SPACE, KEY_SPELLCHECK, KEY_SPORT,
	KEY_SPREADSHEET, KEY_STOP, KEY_STOPCD, KEY_STOP_RECORD, KEY_SUBTITLE, KEY_SUSPEND,
	KEY_SWITCHVIDEOMODE, KEY_SYSRQ, KEY_T, KEY_TAB, KEY_TAPE, KEY_TASKMANAGER, KEY_TEEN, KEY_TEXT,
	KEY_TIME, KEY_TITLE, KEY_TOUCHPAD_OFF, KEY_TOUCHPAD_ON, KEY_TOUCHPAD_TOGGLE,
	KEY_TRADITIONAL_SONAR, KEY_TUNER, KEY_TV, KEY_TV2, KEY_TWEN, KEY_U, KEY_UNDO, KEY_UNKNOWN,
	KEY_UNMUTE, KEY_UP, KEY_UWB, KEY_V, KEY_VCR, KEY_VCR2, KEY_VENDOR, KEY_VIDEO, KEY_VIDEOPHONE,
	KEY_VIDEO_NEXT, KEY_VIDEO_PREV, KEY_VOD, KEY_VOICECOMMAND, KEY_VOICEMAIL, KEY_VOLUMEDOWN,
	KEY_VOLUMEUP, KEY_W, KEY_WAKEUP, KEY_WIMAX, KEY_WLAN, KEY_WORDPROCESSOR, KEY_WPS_BUTTON,
	KEY_WWAN, KEY_WWW, KEY_X, KEY_XFER, KEY_Y, KEY_YELLOW, KEY_YEN, KEY_Z, KEY_ZENKAKUHANKAKU,
	KEY_ZOOM, KEY_ZOOMIN, KEY_ZOOMOUT, KEY_ZOOMRESET,
};

#[test]
fn test_KEY_RESERVED() {
	assert_eq!(KEY_RESERVED!(), 0);
}

#[test]
fn test_KEY_ESC() {
	assert_eq!(KEY_ESC!(), 1);
}

#[test]
fn test_KEY_1() {
	assert_eq!(KEY_1!(), 2);
}

#[test]
fn test_KEY_2() {
	assert_eq!(KEY_2!(), 3);
}

#[test]
fn test_KEY_3() {
	assert_eq!(KEY_3!(), 4);
}

#[test]
fn test_KEY_4() {
	assert_eq!(KEY_4!(), 5);
}

#[test]
fn test_KEY_5() {
	assert_eq!(KEY_5!(), 6);
}

#[test]
fn test_KEY_6() {
	assert_eq!(KEY_6!(), 7);
}

#[test]
fn test_KEY_7() {
	assert_eq!(KEY_7!(), 8);
}

#[test]
fn test_KEY_8() {
	assert_eq!(KEY_8!(), 9);
}

#[test]
fn test_KEY_9() {
	assert_eq!(KEY_9!(), 10);
}

#[test]
fn test_KEY_0() {
	assert_eq!(KEY_0!(), 11);
}

#[test]
fn test_KEY_MINUS() {
	assert_eq!(KEY_MINUS!(), 12);
}

#[test]
fn test_KEY_EQUAL() {
	assert_eq!(KEY_EQUAL!(), 13);
}

#[test]
fn test_KEY_BACKSPACE() {
	assert_eq!(KEY_BACKSPACE!(), 14);
}

#[test]
fn test_KEY_TAB() {
	assert_eq!(KEY_TAB!(), 15);
}

#[test]
fn test_KEY_Q() {
	assert_eq!(KEY_Q!(), 16);
}

#[test]
fn test_KEY_W() {
	assert_eq!(KEY_W!(), 17);
}

#[test]
fn test_KEY_E() {
	assert_eq!(KEY_E!(), 18);
}

#[test]
fn test_KEY_R() {
	assert_eq!(KEY_R!(), 19);
}

#[test]
fn test_KEY_T() {
	assert_eq!(KEY_T!(), 20);
}

#[test]
fn test_KEY_Y() {
	assert_eq!(KEY_Y!(), 21);
}

#[test]
fn test_KEY_U() {
	assert_eq!(KEY_U!(), 22);
}

#[test]
fn test_KEY_I() {
	assert_eq!(KEY_I!(), 23);
}

#[test]
fn test_KEY_O() {
	assert_eq!(KEY_O!(), 24);
}

#[test]
fn test_KEY_P() {
	assert_eq!(KEY_P!(), 25);
}

#[test]
fn test_KEY_LEFTBRACE() {
	assert_eq!(KEY_LEFTBRACE!(), 26);
}

#[test]
fn test_KEY_RIGHTBRACE() {
	assert_eq!(KEY_RIGHTBRACE!(), 27);
}

#[test]
fn test_KEY_ENTER() {
	assert_eq!(KEY_ENTER!(), 28);
}

#[test]
fn test_KEY_LEFTCTRL() {
	assert_eq!(KEY_LEFTCTRL!(), 29);
}

#[test]
fn test_KEY_A() {
	assert_eq!(KEY_A!(), 30);
}

#[test]
fn test_KEY_S() {
	assert_eq!(KEY_S!(), 31);
}

#[test]
fn test_KEY_D() {
	assert_eq!(KEY_D!(), 32);
}

#[test]
fn test_KEY_F() {
	assert_eq!(KEY_F!(), 33);
}

#[test]
fn test_KEY_G() {
	assert_eq!(KEY_G!(), 34);
}

#[test]
fn test_KEY_H() {
	assert_eq!(KEY_H!(), 35);
}

#[test]
fn test_KEY_J() {
	assert_eq!(KEY_J!(), 36);
}

#[test]
fn test_KEY_K() {
	assert_eq!(KEY_K!(), 37);
}

#[test]
fn test_KEY_L() {
	assert_eq!(KEY_L!(), 38);
}

#[test]
fn test_KEY_SEMICOLON() {
	assert_eq!(KEY_SEMICOLON!(), 39);
}

#[test]
fn test_KEY_APOSTROPHE() {
	assert_eq!(KEY_APOSTROPHE!(), 40);
}

#[test]
fn test_KEY_GRAVE() {
	assert_eq!(KEY_GRAVE!(), 41);
}

#[test]
fn test_KEY_LEFTSHIFT() {
	assert_eq!(KEY_LEFTSHIFT!(), 42);
}

#[test]
fn test_KEY_BACKSLASH() {
	assert_eq!(KEY_BACKSLASH!(), 43);
}

#[test]
fn test_KEY_Z() {
	assert_eq!(KEY_Z!(), 44);
}

#[test]
fn test_KEY_X() {
	assert_eq!(KEY_X!(), 45);
}

#[test]
fn test_KEY_C() {
	assert_eq!(KEY_C!(), 46);
}

#[test]
fn test_KEY_V() {
	assert_eq!(KEY_V!(), 47);
}

#[test]
fn test_KEY_B() {
	assert_eq!(KEY_B!(), 48);
}

#[test]
fn test_KEY_N() {
	assert_eq!(KEY_N!(), 49);
}

#[test]
fn test_KEY_M() {
	assert_eq!(KEY_M!(), 50);
}

#[test]
fn test_KEY_COMMA() {
	assert_eq!(KEY_COMMA!(), 51);
}

#[test]
fn test_KEY_DOT() {
	assert_eq!(KEY_DOT!(), 52);
}

#[test]
fn test_KEY_SLASH() {
	assert_eq!(KEY_SLASH!(), 53);
}

#[test]
fn test_KEY_RIGHTSHIFT() {
	assert_eq!(KEY_RIGHTSHIFT!(), 54);
}

#[test]
fn test_KEY_KPASTERISK() {
	assert_eq!(KEY_KPASTERISK!(), 55);
}

#[test]
fn test_KEY_LEFTALT() {
	assert_eq!(KEY_LEFTALT!(), 56);
}

#[test]
fn test_KEY_SPACE() {
	assert_eq!(KEY_SPACE!(), 57);
}

#[test]
fn test_KEY_CAPSLOCK() {
	assert_eq!(KEY_CAPSLOCK!(), 58);
}

#[test]
fn test_KEY_F1() {
	assert_eq!(KEY_F1!(), 59);
}

#[test]
fn test_KEY_F2() {
	assert_eq!(KEY_F2!(), 60);
}

#[test]
fn test_KEY_F3() {
	assert_eq!(KEY_F3!(), 61);
}

#[test]
fn test_KEY_F4() {
	assert_eq!(KEY_F4!(), 62);
}

#[test]
fn test_KEY_F5() {
	assert_eq!(KEY_F5!(), 63);
}

#[test]
fn test_KEY_F6() {
	assert_eq!(KEY_F6!(), 64);
}

#[test]
fn test_KEY_F7() {
	assert_eq!(KEY_F7!(), 65);
}

#[test]
fn test_KEY_F8() {
	assert_eq!(KEY_F8!(), 66);
}

#[test]
fn test_KEY_F9() {
	assert_eq!(KEY_F9!(), 67);
}

#[test]
fn test_KEY_F10() {
	assert_eq!(KEY_F10!(), 68);
}

#[test]
fn test_KEY_NUMLOCK() {
	assert_eq!(KEY_NUMLOCK!(), 69);
}

#[test]
fn test_KEY_SCROLLLOCK() {
	assert_eq!(KEY_SCROLLLOCK!(), 70);
}

#[test]
fn test_KEY_KP7() {
	assert_eq!(KEY_KP7!(), 71);
}

#[test]
fn test_KEY_KP8() {
	assert_eq!(KEY_KP8!(), 72);
}

#[test]
fn test_KEY_KP9() {
	assert_eq!(KEY_KP9!(), 73);
}

#[test]
fn test_KEY_KPMINUS() {
	assert_eq!(KEY_KPMINUS!(), 74);
}

#[test]
fn test_KEY_KP4() {
	assert_eq!(KEY_KP4!(), 75);
}

#[test]
fn test_KEY_KP5() {
	assert_eq!(KEY_KP5!(), 76);
}

#[test]
fn test_KEY_KP6() {
	assert_eq!(KEY_KP6!(), 77);
}

#[test]
fn test_KEY_KPPLUS() {
	assert_eq!(KEY_KPPLUS!(), 78);
}

#[test]
fn test_KEY_KP1() {
	assert_eq!(KEY_KP1!(), 79);
}

#[test]
fn test_KEY_KP2() {
	assert_eq!(KEY_KP2!(), 80);
}

#[test]
fn test_KEY_KP3() {
	assert_eq!(KEY_KP3!(), 81);
}

#[test]
fn test_KEY_KP0() {
	assert_eq!(KEY_KP0!(), 82);
}

#[test]
fn test_KEY_KPDOT() {
	assert_eq!(KEY_KPDOT!(), 83);
}

#[test]
fn test_KEY_ZENKAKUHANKAKU() {
	assert_eq!(KEY_ZENKAKUHANKAKU!(), 85);
}

#[test]
fn test_KEY_102ND() {
	assert_eq!(KEY_102ND!(), 86);
}

#[test]
fn test_KEY_F11() {
	assert_eq!(KEY_F11!(), 87);
}

#[test]
fn test_KEY_F12() {
	assert_eq!(KEY_F12!(), 88);
}

#[test]
fn test_KEY_RO() {
	assert_eq!(KEY_RO!(), 89);
}

#[test]
fn test_KEY_KATAKANA() {
	assert_eq!(KEY_KATAKANA!(), 90);
}

#[test]
fn test_KEY_HIRAGANA() {
	assert_eq!(KEY_HIRAGANA!(), 91);
}

#[test]
fn test_KEY_HENKAN() {
	assert_eq!(KEY_HENKAN!(), 92);
}

#[test]
fn test_KEY_KATAKANAHIRAGANA() {
	assert_eq!(KEY_KATAKANAHIRAGANA!(), 93);
}

#[test]
fn test_KEY_MUHENKAN() {
	assert_eq!(KEY_MUHENKAN!(), 94);
}

#[test]
fn test_KEY_KPJPCOMMA() {
	assert_eq!(KEY_KPJPCOMMA!(), 95);
}

#[test]
fn test_KEY_KPENTER() {
	assert_eq!(KEY_KPENTER!(), 96);
}

#[test]
fn test_KEY_RIGHTCTRL() {
	assert_eq!(KEY_RIGHTCTRL!(), 97);
}

#[test]
fn test_KEY_KPSLASH() {
	assert_eq!(KEY_KPSLASH!(), 98);
}

#[test]
fn test_KEY_SYSRQ() {
	assert_eq!(KEY_SYSRQ!(), 99);
}

#[test]
fn test_KEY_RIGHTALT() {
	assert_eq!(KEY_RIGHTALT!(), 100);
}

#[test]
fn test_KEY_LINEFEED() {
	assert_eq!(KEY_LINEFEED!(), 101);
}

#[test]
fn test_KEY_HOME() {
	assert_eq!(KEY_HOME!(), 102);
}

#[test]
fn test_KEY_UP() {
	assert_eq!(KEY_UP!(), 103);
}

#[test]
fn test_KEY_PAGEUP() {
	assert_eq!(KEY_PAGEUP!(), 104);
}

#[test]
fn test_KEY_LEFT() {
	assert_eq!(KEY_LEFT!(), 105);
}

#[test]
fn test_KEY_RIGHT() {
	assert_eq!(KEY_RIGHT!(), 106);
}

#[test]
fn test_KEY_END() {
	assert_eq!(KEY_END!(), 107);
}

#[test]
fn test_KEY_DOWN() {
	assert_eq!(KEY_DOWN!(), 108);
}

#[test]
fn test_KEY_PAGEDOWN() {
	assert_eq!(KEY_PAGEDOWN!(), 109);
}

#[test]
fn test_KEY_INSERT() {
	assert_eq!(KEY_INSERT!(), 110);
}

#[test]
fn test_KEY_DELETE() {
	assert_eq!(KEY_DELETE!(), 111);
}

#[test]
fn test_KEY_MACRO() {
	assert_eq!(KEY_MACRO!(), 112);
}

#[test]
fn test_KEY_MUTE() {
	assert_eq!(KEY_MUTE!(), 113);
}

#[test]
fn test_KEY_VOLUMEDOWN() {
	assert_eq!(KEY_VOLUMEDOWN!(), 114);
}

#[test]
fn test_KEY_VOLUMEUP() {
	assert_eq!(KEY_VOLUMEUP!(), 115);
}

#[test]
fn test_KEY_POWER() {
	assert_eq!(KEY_POWER!(), 116);
}

#[test]
fn test_KEY_KPEQUAL() {
	assert_eq!(KEY_KPEQUAL!(), 117);
}

#[test]
fn test_KEY_KPPLUSMINUS() {
	assert_eq!(KEY_KPPLUSMINUS!(), 118);
}

#[test]
fn test_KEY_PAUSE() {
	assert_eq!(KEY_PAUSE!(), 119);
}

#[test]
fn test_KEY_SCALE() {
	assert_eq!(KEY_SCALE!(), 120);
}

#[test]
fn test_KEY_KPCOMMA() {
	assert_eq!(KEY_KPCOMMA!(), 121);
}

#[test]
fn test_KEY_HANGEUL() {
	assert_eq!(KEY_HANGEUL!(), 122);
}

#[test]
fn test_KEY_HANGUEL() {
	assert_eq!(KEY_HANGUEL!(), KEY_HANGEUL!());
}

#[test]
fn test_KEY_HANJA() {
	assert_eq!(KEY_HANJA!(), 123);
}

#[test]
fn test_KEY_YEN() {
	assert_eq!(KEY_YEN!(), 124);
}

#[test]
fn test_KEY_LEFTMETA() {
	assert_eq!(KEY_LEFTMETA!(), 125);
}

#[test]
fn test_KEY_RIGHTMETA() {
	assert_eq!(KEY_RIGHTMETA!(), 126);
}

#[test]
fn test_KEY_COMPOSE() {
	assert_eq!(KEY_COMPOSE!(), 127);
}

#[test]
fn test_KEY_STOP() {
	assert_eq!(KEY_STOP!(), 128);
}

#[test]
fn test_KEY_AGAIN() {
	assert_eq!(KEY_AGAIN!(), 129);
}

#[test]
fn test_KEY_PROPS() {
	assert_eq!(KEY_PROPS!(), 130);
}

#[test]
fn test_KEY_UNDO() {
	assert_eq!(KEY_UNDO!(), 131);
}

#[test]
fn test_KEY_FRONT() {
	assert_eq!(KEY_FRONT!(), 132);
}

#[test]
fn test_KEY_COPY() {
	assert_eq!(KEY_COPY!(), 133);
}

#[test]
fn test_KEY_OPEN() {
	assert_eq!(KEY_OPEN!(), 134);
}

#[test]
fn test_KEY_PASTE() {
	assert_eq!(KEY_PASTE!(), 135);
}

#[test]
fn test_KEY_FIND() {
	assert_eq!(KEY_FIND!(), 136);
}

#[test]
fn test_KEY_CUT() {
	assert_eq!(KEY_CUT!(), 137);
}

#[test]
fn test_KEY_HELP() {
	assert_eq!(KEY_HELP!(), 138);
}

#[test]
fn test_KEY_MENU() {
	assert_eq!(KEY_MENU!(), 139);
}

#[test]
fn test_KEY_CALC() {
	assert_eq!(KEY_CALC!(), 140);
}

#[test]
fn test_KEY_SETUP() {
	assert_eq!(KEY_SETUP!(), 141);
}

#[test]
fn test_KEY_SLEEP() {
	assert_eq!(KEY_SLEEP!(), 142);
}

#[test]
fn test_KEY_WAKEUP() {
	assert_eq!(KEY_WAKEUP!(), 143);
}

#[test]
fn test_KEY_FILE() {
	assert_eq!(KEY_FILE!(), 144);
}

#[test]
fn test_KEY_SENDFILE() {
	assert_eq!(KEY_SENDFILE!(), 145);
}

#[test]
fn test_KEY_DELETEFILE() {
	assert_eq!(KEY_DELETEFILE!(), 146);
}

#[test]
fn test_KEY_XFER() {
	assert_eq!(KEY_XFER!(), 147);
}

#[test]
fn test_KEY_PROG1() {
	assert_eq!(KEY_PROG1!(), 148);
}

#[test]
fn test_KEY_PROG2() {
	assert_eq!(KEY_PROG2!(), 149);
}

#[test]
fn test_KEY_WWW() {
	assert_eq!(KEY_WWW!(), 150);
}

#[test]
fn test_KEY_MSDOS() {
	assert_eq!(KEY_MSDOS!(), 151);
}

#[test]
fn test_KEY_COFFEE() {
	assert_eq!(KEY_COFFEE!(), 152);
}

#[test]
fn test_KEY_SCREENLOCK() {
	assert_eq!(KEY_SCREENLOCK!(), KEY_COFFEE!());
}

#[test]
fn test_KEY_ROTATE_DISPLAY() {
	assert_eq!(KEY_ROTATE_DISPLAY!(), 153);
}

#[test]
fn test_KEY_DIRECTION() {
	assert_eq!(KEY_DIRECTION!(), KEY_ROTATE_DISPLAY!());
}

#[test]
fn test_KEY_CYCLEWINDOWS() {
	assert_eq!(KEY_CYCLEWINDOWS!(), 154);
}

#[test]
fn test_KEY_MAIL() {
	assert_eq!(KEY_MAIL!(), 155);
}

#[test]
fn test_KEY_BOOKMARKS() {
	assert_eq!(KEY_BOOKMARKS!(), 156);
}

#[test]
fn test_KEY_COMPUTER() {
	assert_eq!(KEY_COMPUTER!(), 157);
}

#[test]
fn test_KEY_BACK() {
	assert_eq!(KEY_BACK!(), 158);
}

#[test]
fn test_KEY_FORWARD() {
	assert_eq!(KEY_FORWARD!(), 159);
}

#[test]
fn test_KEY_CLOSECD() {
	assert_eq!(KEY_CLOSECD!(), 160);
}

#[test]
fn test_KEY_EJECTCD() {
	assert_eq!(KEY_EJECTCD!(), 161);
}

#[test]
fn test_KEY_EJECTCLOSECD() {
	assert_eq!(KEY_EJECTCLOSECD!(), 162);
}

#[test]
fn test_KEY_NEXTSONG() {
	assert_eq!(KEY_NEXTSONG!(), 163);
}

#[test]
fn test_KEY_PLAYPAUSE() {
	assert_eq!(KEY_PLAYPAUSE!(), 164);
}

#[test]
fn test_KEY_PREVIOUSSONG() {
	assert_eq!(KEY_PREVIOUSSONG!(), 165);
}

#[test]
fn test_KEY_STOPCD() {
	assert_eq!(KEY_STOPCD!(), 166);
}

#[test]
fn test_KEY_RECORD() {
	assert_eq!(KEY_RECORD!(), 167);
}

#[test]
fn test_KEY_REWIND() {
	assert_eq!(KEY_REWIND!(), 168);
}

#[test]
fn test_KEY_PHONE() {
	assert_eq!(KEY_PHONE!(), 169);
}

#[test]
fn test_KEY_ISO() {
	assert_eq!(KEY_ISO!(), 170);
}

#[test]
fn test_KEY_CONFIG() {
	assert_eq!(KEY_CONFIG!(), 171);
}

#[test]
fn test_KEY_HOMEPAGE() {
	assert_eq!(KEY_HOMEPAGE!(), 172);
}

#[test]
fn test_KEY_REFRESH() {
	assert_eq!(KEY_REFRESH!(), 173);
}

#[test]
fn test_KEY_EXIT() {
	assert_eq!(KEY_EXIT!(), 174);
}

#[test]
fn test_KEY_MOVE() {
	assert_eq!(KEY_MOVE!(), 175);
}

#[test]
fn test_KEY_EDIT() {
	assert_eq!(KEY_EDIT!(), 176);
}

#[test]
fn test_KEY_SCROLLUP() {
	assert_eq!(KEY_SCROLLUP!(), 177);
}

#[test]
fn test_KEY_SCROLLDOWN() {
	assert_eq!(KEY_SCROLLDOWN!(), 178);
}

#[test]
fn test_KEY_KPLEFTPAREN() {
	assert_eq!(KEY_KPLEFTPAREN!(), 179);
}

#[test]
fn test_KEY_KPRIGHTPAREN() {
	assert_eq!(KEY_KPRIGHTPAREN!(), 180);
}

#[test]
fn test_KEY_NEW() {
	assert_eq!(KEY_NEW!(), 181);
}

#[test]
fn test_KEY_REDO() {
	assert_eq!(KEY_REDO!(), 182);
}

#[test]
fn test_KEY_F13() {
	assert_eq!(KEY_F13!(), 183);
}

#[test]
fn test_KEY_F14() {
	assert_eq!(KEY_F14!(), 184);
}

#[test]
fn test_KEY_F15() {
	assert_eq!(KEY_F15!(), 185);
}

#[test]
fn test_KEY_F16() {
	assert_eq!(KEY_F16!(), 186);
}

#[test]
fn test_KEY_F17() {
	assert_eq!(KEY_F17!(), 187);
}

#[test]
fn test_KEY_F18() {
	assert_eq!(KEY_F18!(), 188);
}

#[test]
fn test_KEY_F19() {
	assert_eq!(KEY_F19!(), 189);
}

#[test]
fn test_KEY_F20() {
	assert_eq!(KEY_F20!(), 190);
}

#[test]
fn test_KEY_F21() {
	assert_eq!(KEY_F21!(), 191);
}

#[test]
fn test_KEY_F22() {
	assert_eq!(KEY_F22!(), 192);
}

#[test]
fn test_KEY_F23() {
	assert_eq!(KEY_F23!(), 193);
}

#[test]
fn test_KEY_F24() {
	assert_eq!(KEY_F24!(), 194);
}

#[test]
fn test_KEY_PLAYCD() {
	assert_eq!(KEY_PLAYCD!(), 200);
}

#[test]
fn test_KEY_PAUSECD() {
	assert_eq!(KEY_PAUSECD!(), 201);
}

#[test]
fn test_KEY_PROG3() {
	assert_eq!(KEY_PROG3!(), 202);
}

#[test]
fn test_KEY_PROG4() {
	assert_eq!(KEY_PROG4!(), 203);
}

#[test]
fn test_KEY_ALL_APPLICATIONS() {
	assert_eq!(KEY_ALL_APPLICATIONS!(), 204);
}

#[test]
fn test_KEY_DASHBOARD() {
	assert_eq!(KEY_DASHBOARD!(), KEY_ALL_APPLICATIONS!());
}

#[test]
fn test_KEY_SUSPEND() {
	assert_eq!(KEY_SUSPEND!(), 205);
}

#[test]
fn test_KEY_CLOSE() {
	assert_eq!(KEY_CLOSE!(), 206);
}

#[test]
fn test_KEY_PLAY() {
	assert_eq!(KEY_PLAY!(), 207);
}

#[test]
fn test_KEY_FASTFORWARD() {
	assert_eq!(KEY_FASTFORWARD!(), 208);
}

#[test]
fn test_KEY_BASSBOOST() {
	assert_eq!(KEY_BASSBOOST!(), 209);
}

#[test]
fn test_KEY_PRINT() {
	assert_eq!(KEY_PRINT!(), 210);
}

#[test]
fn test_KEY_HP() {
	assert_eq!(KEY_HP!(), 211);
}

#[test]
fn test_KEY_CAMERA() {
	assert_eq!(KEY_CAMERA!(), 212);
}

#[test]
fn test_KEY_SOUND() {
	assert_eq!(KEY_SOUND!(), 213);
}

#[test]
fn test_KEY_QUESTION() {
	assert_eq!(KEY_QUESTION!(), 214);
}

#[test]
fn test_KEY_EMAIL() {
	assert_eq!(KEY_EMAIL!(), 215);
}

#[test]
fn test_KEY_CHAT() {
	assert_eq!(KEY_CHAT!(), 216);
}

#[test]
fn test_KEY_SEARCH() {
	assert_eq!(KEY_SEARCH!(), 217);
}

#[test]
fn test_KEY_CONNECT() {
	assert_eq!(KEY_CONNECT!(), 218);
}

#[test]
fn test_KEY_FINANCE() {
	assert_eq!(KEY_FINANCE!(), 219);
}

#[test]
fn test_KEY_SPORT() {
	assert_eq!(KEY_SPORT!(), 220);
}

#[test]
fn test_KEY_SHOP() {
	assert_eq!(KEY_SHOP!(), 221);
}

#[test]
fn test_KEY_ALTERASE() {
	assert_eq!(KEY_ALTERASE!(), 222);
}

#[test]
fn test_KEY_CANCEL() {
	assert_eq!(KEY_CANCEL!(), 223);
}

#[test]
fn test_KEY_BRIGHTNESSDOWN() {
	assert_eq!(KEY_BRIGHTNESSDOWN!(), 224);
}

#[test]
fn test_KEY_BRIGHTNESSUP() {
	assert_eq!(KEY_BRIGHTNESSUP!(), 225);
}

#[test]
fn test_KEY_MEDIA() {
	assert_eq!(KEY_MEDIA!(), 226);
}

#[test]
fn test_KEY_SWITCHVIDEOMODE() {
	assert_eq!(KEY_SWITCHVIDEOMODE!(), 227);
}

#[test]
fn test_KEY_KBDILLUMTOGGLE() {
	assert_eq!(KEY_KBDILLUMTOGGLE!(), 228);
}

#[test]
fn test_KEY_KBDILLUMDOWN() {
	assert_eq!(KEY_KBDILLUMDOWN!(), 229);
}

#[test]
fn test_KEY_KBDILLUMUP() {
	assert_eq!(KEY_KBDILLUMUP!(), 230);
}

#[test]
fn test_KEY_SEND() {
	assert_eq!(KEY_SEND!(), 231);
}

#[test]
fn test_KEY_REPLY() {
	assert_eq!(KEY_REPLY!(), 232);
}

#[test]
fn test_KEY_FORWARDMAIL() {
	assert_eq!(KEY_FORWARDMAIL!(), 233);
}

#[test]
fn test_KEY_SAVE() {
	assert_eq!(KEY_SAVE!(), 234);
}

#[test]
fn test_KEY_DOCUMENTS() {
	assert_eq!(KEY_DOCUMENTS!(), 235);
}

#[test]
fn test_KEY_BATTERY() {
	assert_eq!(KEY_BATTERY!(), 236);
}

#[test]
fn test_KEY_BLUETOOTH() {
	assert_eq!(KEY_BLUETOOTH!(), 237);
}

#[test]
fn test_KEY_WLAN() {
	assert_eq!(KEY_WLAN!(), 238);
}

#[test]
fn test_KEY_UWB() {
	assert_eq!(KEY_UWB!(), 239);
}

#[test]
fn test_KEY_UNKNOWN() {
	assert_eq!(KEY_UNKNOWN!(), 240);
}

#[test]
fn test_KEY_VIDEO_NEXT() {
	assert_eq!(KEY_VIDEO_NEXT!(), 241);
}

#[test]
fn test_KEY_VIDEO_PREV() {
	assert_eq!(KEY_VIDEO_PREV!(), 242);
}

#[test]
fn test_KEY_BRIGHTNESS_CYCLE() {
	assert_eq!(KEY_BRIGHTNESS_CYCLE!(), 243);
}

#[test]
fn test_KEY_BRIGHTNESS_AUTO() {
	assert_eq!(KEY_BRIGHTNESS_AUTO!(), 244);
}

#[test]
fn test_KEY_BRIGHTNESS_ZERO() {
	assert_eq!(KEY_BRIGHTNESS_ZERO!(), KEY_BRIGHTNESS_AUTO!());
}

#[test]
fn test_KEY_DISPLAY_OFF() {
	assert_eq!(KEY_DISPLAY_OFF!(), 245);
}

#[test]
fn test_KEY_WWAN() {
	assert_eq!(KEY_WWAN!(), 246);
}

#[test]
fn test_KEY_WIMAX() {
	assert_eq!(KEY_WIMAX!(), KEY_WWAN!());
}

#[test]
fn test_KEY_RFKILL() {
	assert_eq!(KEY_RFKILL!(), 247);
}

#[test]
fn test_KEY_MICMUTE() {
	assert_eq!(KEY_MICMUTE!(), 248);
}

#[test]
fn test_BTN_MISC() {
	assert_eq!(BTN_MISC!(), 0x100);
}

#[test]
fn test_BTN_0() {
	assert_eq!(BTN_0!(), 0x100);
}

#[test]
fn test_BTN_1() {
	assert_eq!(BTN_1!(), 0x101);
}

#[test]
fn test_BTN_2() {
	assert_eq!(BTN_2!(), 0x102);
}

#[test]
fn test_BTN_3() {
	assert_eq!(BTN_3!(), 0x103);
}

#[test]
fn test_BTN_4() {
	assert_eq!(BTN_4!(), 0x104);
}

#[test]
fn test_BTN_5() {
	assert_eq!(BTN_5!(), 0x105);
}

#[test]
fn test_BTN_6() {
	assert_eq!(BTN_6!(), 0x106);
}

#[test]
fn test_BTN_7() {
	assert_eq!(BTN_7!(), 0x107);
}

#[test]
fn test_BTN_8() {
	assert_eq!(BTN_8!(), 0x108);
}

#[test]
fn test_BTN_9() {
	assert_eq!(BTN_9!(), 0x109);
}

#[test]
fn test_BTN_MOUSE() {
	assert_eq!(BTN_MOUSE!(), 0x110);
}

#[test]
fn test_BTN_LEFT() {
	assert_eq!(BTN_LEFT!(), 0x110);
}

#[test]
fn test_BTN_RIGHT() {
	assert_eq!(BTN_RIGHT!(), 0x111);
}

#[test]
fn test_BTN_MIDDLE() {
	assert_eq!(BTN_MIDDLE!(), 0x112);
}

#[test]
fn test_BTN_SIDE() {
	assert_eq!(BTN_SIDE!(), 0x113);
}

#[test]
fn test_BTN_EXTRA() {
	assert_eq!(BTN_EXTRA!(), 0x114);
}

#[test]
fn test_BTN_FORWARD() {
	assert_eq!(BTN_FORWARD!(), 0x115);
}

#[test]
fn test_BTN_BACK() {
	assert_eq!(BTN_BACK!(), 0x116);
}

#[test]
fn test_BTN_TASK() {
	assert_eq!(BTN_TASK!(), 0x117);
}

#[test]
fn test_BTN_JOYSTICK() {
	assert_eq!(BTN_JOYSTICK!(), 0x120);
}

#[test]
fn test_BTN_TRIGGER() {
	assert_eq!(BTN_TRIGGER!(), 0x120);
}

#[test]
fn test_BTN_THUMB() {
	assert_eq!(BTN_THUMB!(), 0x121);
}

#[test]
fn test_BTN_THUMB2() {
	assert_eq!(BTN_THUMB2!(), 0x122);
}

#[test]
fn test_BTN_TOP() {
	assert_eq!(BTN_TOP!(), 0x123);
}

#[test]
fn test_BTN_TOP2() {
	assert_eq!(BTN_TOP2!(), 0x124);
}

#[test]
fn test_BTN_PINKIE() {
	assert_eq!(BTN_PINKIE!(), 0x125);
}

#[test]
fn test_BTN_BASE() {
	assert_eq!(BTN_BASE!(), 0x126);
}

#[test]
fn test_BTN_BASE2() {
	assert_eq!(BTN_BASE2!(), 0x127);
}

#[test]
fn test_BTN_BASE3() {
	assert_eq!(BTN_BASE3!(), 0x128);
}

#[test]
fn test_BTN_BASE4() {
	assert_eq!(BTN_BASE4!(), 0x129);
}

#[test]
fn test_BTN_BASE5() {
	assert_eq!(BTN_BASE5!(), 0x12a);
}

#[test]
fn test_BTN_BASE6() {
	assert_eq!(BTN_BASE6!(), 0x12b);
}

#[test]
fn test_BTN_DEAD() {
	assert_eq!(BTN_DEAD!(), 0x12f);
}

#[test]
fn test_BTN_GAMEPAD() {
	assert_eq!(BTN_GAMEPAD!(), 0x130);
}

#[test]
fn test_BTN_SOUTH() {
	assert_eq!(BTN_SOUTH!(), 0x130);
}

#[test]
fn test_BTN_A() {
	assert_eq!(BTN_A!(), BTN_SOUTH!());
}

#[test]
fn test_BTN_EAST() {
	assert_eq!(BTN_EAST!(), 0x131);
}

#[test]
fn test_BTN_B() {
	assert_eq!(BTN_B!(), BTN_EAST!());
}

#[test]
fn test_BTN_C() {
	assert_eq!(BTN_C!(), 0x132);
}

#[test]
fn test_BTN_NORTH() {
	assert_eq!(BTN_NORTH!(), 0x133);
}

#[test]
fn test_BTN_X() {
	assert_eq!(BTN_X!(), BTN_NORTH!());
}

#[test]
fn test_BTN_WEST() {
	assert_eq!(BTN_WEST!(), 0x134);
}

#[test]
fn test_BTN_Y() {
	assert_eq!(BTN_Y!(), BTN_WEST!());
}

#[test]
fn test_BTN_Z() {
	assert_eq!(BTN_Z!(), 0x135);
}

#[test]
fn test_BTN_TL() {
	assert_eq!(BTN_TL!(), 0x136);
}

#[test]
fn test_BTN_TR() {
	assert_eq!(BTN_TR!(), 0x137);
}

#[test]
fn test_BTN_TL2() {
	assert_eq!(BTN_TL2!(), 0x138);
}

#[test]
fn test_BTN_TR2() {
	assert_eq!(BTN_TR2!(), 0x139);
}

#[test]
fn test_BTN_SELECT() {
	assert_eq!(BTN_SELECT!(), 0x13a);
}

#[test]
fn test_BTN_START() {
	assert_eq!(BTN_START!(), 0x13b);
}

#[test]
fn test_BTN_MODE() {
	assert_eq!(BTN_MODE!(), 0x13c);
}

#[test]
fn test_BTN_THUMBL() {
	assert_eq!(BTN_THUMBL!(), 0x13d);
}

#[test]
fn test_BTN_THUMBR() {
	assert_eq!(BTN_THUMBR!(), 0x13e);
}

#[test]
fn test_BTN_DIGI() {
	assert_eq!(BTN_DIGI!(), 0x140);
}

#[test]
fn test_BTN_TOOL_PEN() {
	assert_eq!(BTN_TOOL_PEN!(), 0x140);
}

#[test]
fn test_BTN_TOOL_RUBBER() {
	assert_eq!(BTN_TOOL_RUBBER!(), 0x141);
}

#[test]
fn test_BTN_TOOL_BRUSH() {
	assert_eq!(BTN_TOOL_BRUSH!(), 0x142);
}

#[test]
fn test_BTN_TOOL_PENCIL() {
	assert_eq!(BTN_TOOL_PENCIL!(), 0x143);
}

#[test]
fn test_BTN_TOOL_AIRBRUSH() {
	assert_eq!(BTN_TOOL_AIRBRUSH!(), 0x144);
}

#[test]
fn test_BTN_TOOL_FINGER() {
	assert_eq!(BTN_TOOL_FINGER!(), 0x145);
}

#[test]
fn test_BTN_TOOL_MOUSE() {
	assert_eq!(BTN_TOOL_MOUSE!(), 0x146);
}

#[test]
fn test_BTN_TOOL_LENS() {
	assert_eq!(BTN_TOOL_LENS!(), 0x147);
}

#[test]
fn test_BTN_TOOL_QUINTTAP() {
	assert_eq!(BTN_TOOL_QUINTTAP!(), 0x148);
}

#[test]
fn test_BTN_STYLUS3() {
	assert_eq!(BTN_STYLUS3!(), 0x149);
}

#[test]
fn test_BTN_TOUCH() {
	assert_eq!(BTN_TOUCH!(), 0x14a);
}

#[test]
fn test_BTN_STYLUS() {
	assert_eq!(BTN_STYLUS!(), 0x14b);
}

#[test]
fn test_BTN_STYLUS2() {
	assert_eq!(BTN_STYLUS2!(), 0x14c);
}

#[test]
fn test_BTN_TOOL_DOUBLETAP() {
	assert_eq!(BTN_TOOL_DOUBLETAP!(), 0x14d);
}

#[test]
fn test_BTN_TOOL_TRIPLETAP() {
	assert_eq!(BTN_TOOL_TRIPLETAP!(), 0x14e);
}

#[test]
fn test_BTN_TOOL_QUADTAP() {
	assert_eq!(BTN_TOOL_QUADTAP!(), 0x14f);
}

#[test]
fn test_BTN_WHEEL() {
	assert_eq!(BTN_WHEEL!(), 0x150);
}

#[test]
fn test_BTN_GEAR_DOWN() {
	assert_eq!(BTN_GEAR_DOWN!(), 0x150);
}

#[test]
fn test_BTN_GEAR_UP() {
	assert_eq!(BTN_GEAR_UP!(), 0x151);
}

#[test]
fn test_KEY_OK() {
	assert_eq!(KEY_OK!(), 0x160);
}

#[test]
fn test_KEY_SELECT() {
	assert_eq!(KEY_SELECT!(), 0x161);
}

#[test]
fn test_KEY_GOTO() {
	assert_eq!(KEY_GOTO!(), 0x162);
}

#[test]
fn test_KEY_CLEAR() {
	assert_eq!(KEY_CLEAR!(), 0x163);
}

#[test]
fn test_KEY_POWER2() {
	assert_eq!(KEY_POWER2!(), 0x164);
}

#[test]
fn test_KEY_OPTION() {
	assert_eq!(KEY_OPTION!(), 0x165);
}

#[test]
fn test_KEY_INFO() {
	assert_eq!(KEY_INFO!(), 0x166);
}

#[test]
fn test_KEY_TIME() {
	assert_eq!(KEY_TIME!(), 0x167);
}

#[test]
fn test_KEY_VENDOR() {
	assert_eq!(KEY_VENDOR!(), 0x168);
}

#[test]
fn test_KEY_ARCHIVE() {
	assert_eq!(KEY_ARCHIVE!(), 0x169);
}

#[test]
fn test_KEY_PROGRAM() {
	assert_eq!(KEY_PROGRAM!(), 0x16a);
}

#[test]
fn test_KEY_CHANNEL() {
	assert_eq!(KEY_CHANNEL!(), 0x16b);
}

#[test]
fn test_KEY_FAVORITES() {
	assert_eq!(KEY_FAVORITES!(), 0x16c);
}

#[test]
fn test_KEY_EPG() {
	assert_eq!(KEY_EPG!(), 0x16d);
}

#[test]
fn test_KEY_PVR() {
	assert_eq!(KEY_PVR!(), 0x16e);
}

#[test]
fn test_KEY_MHP() {
	assert_eq!(KEY_MHP!(), 0x16f);
}

#[test]
fn test_KEY_LANGUAGE() {
	assert_eq!(KEY_LANGUAGE!(), 0x170);
}

#[test]
fn test_KEY_TITLE() {
	assert_eq!(KEY_TITLE!(), 0x171);
}

#[test]
fn test_KEY_SUBTITLE() {
	assert_eq!(KEY_SUBTITLE!(), 0x172);
}

#[test]
fn test_KEY_ANGLE() {
	assert_eq!(KEY_ANGLE!(), 0x173);
}

#[test]
fn test_KEY_FULL_SCREEN() {
	assert_eq!(KEY_FULL_SCREEN!(), 0x174);
}

#[test]
fn test_KEY_ZOOM() {
	assert_eq!(KEY_ZOOM!(), KEY_FULL_SCREEN!());
}

#[test]
fn test_KEY_MODE() {
	assert_eq!(KEY_MODE!(), 0x175);
}

#[test]
fn test_KEY_KEYBOARD() {
	assert_eq!(KEY_KEYBOARD!(), 0x176);
}

#[test]
fn test_KEY_ASPECT_RATIO() {
	assert_eq!(KEY_ASPECT_RATIO!(), 0x177);
}

#[test]
fn test_KEY_SCREEN() {
	assert_eq!(KEY_SCREEN!(), KEY_ASPECT_RATIO!());
}

#[test]
fn test_KEY_PC() {
	assert_eq!(KEY_PC!(), 0x178);
}

#[test]
fn test_KEY_TV() {
	assert_eq!(KEY_TV!(), 0x179);
}

#[test]
fn test_KEY_TV2() {
	assert_eq!(KEY_TV2!(), 0x17a);
}

#[test]
fn test_KEY_VCR() {
	assert_eq!(KEY_VCR!(), 0x17b);
}

#[test]
fn test_KEY_VCR2() {
	assert_eq!(KEY_VCR2!(), 0x17c);
}

#[test]
fn test_KEY_SAT() {
	assert_eq!(KEY_SAT!(), 0x17d);
}

#[test]
fn test_KEY_SAT2() {
	assert_eq!(KEY_SAT2!(), 0x17e);
}

#[test]
fn test_KEY_CD() {
	assert_eq!(KEY_CD!(), 0x17f);
}

#[test]
fn test_KEY_TAPE() {
	assert_eq!(KEY_TAPE!(), 0x180);
}

#[test]
fn test_KEY_RADIO() {
	assert_eq!(KEY_RADIO!(), 0x181);
}

#[test]
fn test_KEY_TUNER() {
	assert_eq!(KEY_TUNER!(), 0x182);
}

#[test]
fn test_KEY_PLAYER() {
	assert_eq!(KEY_PLAYER!(), 0x183);
}

#[test]
fn test_KEY_TEXT() {
	assert_eq!(KEY_TEXT!(), 0x184);
}

#[test]
fn test_KEY_DVD() {
	assert_eq!(KEY_DVD!(), 0x185);
}

#[test]
fn test_KEY_AUX() {
	assert_eq!(KEY_AUX!(), 0x186);
}

#[test]
fn test_KEY_MP3() {
	assert_eq!(KEY_MP3!(), 0x187);
}

#[test]
fn test_KEY_AUDIO() {
	assert_eq!(KEY_AUDIO!(), 0x188);
}

#[test]
fn test_KEY_VIDEO() {
	assert_eq!(KEY_VIDEO!(), 0x189);
}

#[test]
fn test_KEY_DIRECTORY() {
	assert_eq!(KEY_DIRECTORY!(), 0x18a);
}

#[test]
fn test_KEY_LIST() {
	assert_eq!(KEY_LIST!(), 0x18b);
}

#[test]
fn test_KEY_MEMO() {
	assert_eq!(KEY_MEMO!(), 0x18c);
}

#[test]
fn test_KEY_CALENDAR() {
	assert_eq!(KEY_CALENDAR!(), 0x18d);
}

#[test]
fn test_KEY_RED() {
	assert_eq!(KEY_RED!(), 0x18e);
}

#[test]
fn test_KEY_GREEN() {
	assert_eq!(KEY_GREEN!(), 0x18f);
}

#[test]
fn test_KEY_YELLOW() {
	assert_eq!(KEY_YELLOW!(), 0x190);
}

#[test]
fn test_KEY_BLUE() {
	assert_eq!(KEY_BLUE!(), 0x191);
}

#[test]
fn test_KEY_CHANNELUP() {
	assert_eq!(KEY_CHANNELUP!(), 0x192);
}

#[test]
fn test_KEY_CHANNELDOWN() {
	assert_eq!(KEY_CHANNELDOWN!(), 0x193);
}

#[test]
fn test_KEY_FIRST() {
	assert_eq!(KEY_FIRST!(), 0x194);
}

#[test]
fn test_KEY_LAST() {
	assert_eq!(KEY_LAST!(), 0x195);
}

#[test]
fn test_KEY_AB() {
	assert_eq!(KEY_AB!(), 0x196);
}

#[test]
fn test_KEY_NEXT() {
	assert_eq!(KEY_NEXT!(), 0x197);
}

#[test]
fn test_KEY_RESTART() {
	assert_eq!(KEY_RESTART!(), 0x198);
}

#[test]
fn test_KEY_SLOW() {
	assert_eq!(KEY_SLOW!(), 0x199);
}

#[test]
fn test_KEY_SHUFFLE() {
	assert_eq!(KEY_SHUFFLE!(), 0x19a);
}

#[test]
fn test_KEY_BREAK() {
	assert_eq!(KEY_BREAK!(), 0x19b);
}

#[test]
fn test_KEY_PREVIOUS() {
	assert_eq!(KEY_PREVIOUS!(), 0x19c);
}

#[test]
fn test_KEY_DIGITS() {
	assert_eq!(KEY_DIGITS!(), 0x19d);
}

#[test]
fn test_KEY_TEEN() {
	assert_eq!(KEY_TEEN!(), 0x19e);
}

#[test]
fn test_KEY_TWEN() {
	assert_eq!(KEY_TWEN!(), 0x19f);
}

#[test]
fn test_KEY_VIDEOPHONE() {
	assert_eq!(KEY_VIDEOPHONE!(), 0x1a0);
}

#[test]
fn test_KEY_GAMES() {
	assert_eq!(KEY_GAMES!(), 0x1a1);
}

#[test]
fn test_KEY_ZOOMIN() {
	assert_eq!(KEY_ZOOMIN!(), 0x1a2);
}

#[test]
fn test_KEY_ZOOMOUT() {
	assert_eq!(KEY_ZOOMOUT!(), 0x1a3);
}

#[test]
fn test_KEY_ZOOMRESET() {
	assert_eq!(KEY_ZOOMRESET!(), 0x1a4);
}

#[test]
fn test_KEY_WORDPROCESSOR() {
	assert_eq!(KEY_WORDPROCESSOR!(), 0x1a5);
}

#[test]
fn test_KEY_EDITOR() {
	assert_eq!(KEY_EDITOR!(), 0x1a6);
}

#[test]
fn test_KEY_SPREADSHEET() {
	assert_eq!(KEY_SPREADSHEET!(), 0x1a7);
}

#[test]
fn test_KEY_GRAPHICSEDITOR() {
	assert_eq!(KEY_GRAPHICSEDITOR!(), 0x1a8);
}

#[test]
fn test_KEY_PRESENTATION() {
	assert_eq!(KEY_PRESENTATION!(), 0x1a9);
}

#[test]
fn test_KEY_DATABASE() {
	assert_eq!(KEY_DATABASE!(), 0x1aa);
}

#[test]
fn test_KEY_NEWS() {
	assert_eq!(KEY_NEWS!(), 0x1ab);
}

#[test]
fn test_KEY_VOICEMAIL() {
	assert_eq!(KEY_VOICEMAIL!(), 0x1ac);
}

#[test]
fn test_KEY_ADDRESSBOOK() {
	assert_eq!(KEY_ADDRESSBOOK!(), 0x1ad);
}

#[test]
fn test_KEY_MESSENGER() {
	assert_eq!(KEY_MESSENGER!(), 0x1ae);
}

#[test]
fn test_KEY_DISPLAYTOGGLE() {
	assert_eq!(KEY_DISPLAYTOGGLE!(), 0x1af);
}

#[test]
fn test_KEY_BRIGHTNESS_TOGGLE() {
	assert_eq!(KEY_BRIGHTNESS_TOGGLE!(), KEY_DISPLAYTOGGLE!());
}

#[test]
fn test_KEY_SPELLCHECK() {
	assert_eq!(KEY_SPELLCHECK!(), 0x1b0);
}

#[test]
fn test_KEY_LOGOFF() {
	assert_eq!(KEY_LOGOFF!(), 0x1b1);
}

#[test]
fn test_KEY_DOLLAR() {
	assert_eq!(KEY_DOLLAR!(), 0x1b2);
}

#[test]
fn test_KEY_EURO() {
	assert_eq!(KEY_EURO!(), 0x1b3);
}

#[test]
fn test_KEY_FRAMEBACK() {
	assert_eq!(KEY_FRAMEBACK!(), 0x1b4);
}

#[test]
fn test_KEY_FRAMEFORWARD() {
	assert_eq!(KEY_FRAMEFORWARD!(), 0x1b5);
}

#[test]
fn test_KEY_CONTEXT_MENU() {
	assert_eq!(KEY_CONTEXT_MENU!(), 0x1b6);
}

#[test]
fn test_KEY_MEDIA_REPEAT() {
	assert_eq!(KEY_MEDIA_REPEAT!(), 0x1b7);
}

#[test]
fn test_KEY_10CHANNELSUP() {
	assert_eq!(KEY_10CHANNELSUP!(), 0x1b8);
}

#[test]
fn test_KEY_10CHANNELSDOWN() {
	assert_eq!(KEY_10CHANNELSDOWN!(), 0x1b9);
}

#[test]
fn test_KEY_IMAGES() {
	assert_eq!(KEY_IMAGES!(), 0x1ba);
}

#[test]
fn test_KEY_NOTIFICATION_CENTER() {
	assert_eq!(KEY_NOTIFICATION_CENTER!(), 0x1bc);
}

#[test]
fn test_KEY_PICKUP_PHONE() {
	assert_eq!(KEY_PICKUP_PHONE!(), 0x1bd);
}

#[test]
fn test_KEY_HANGUP_PHONE() {
	assert_eq!(KEY_HANGUP_PHONE!(), 0x1be);
}

#[test]
fn test_KEY_DEL_EOL() {
	assert_eq!(KEY_DEL_EOL!(), 0x1c0);
}

#[test]
fn test_KEY_DEL_EOS() {
	assert_eq!(KEY_DEL_EOS!(), 0x1c1);
}

#[test]
fn test_KEY_INS_LINE() {
	assert_eq!(KEY_INS_LINE!(), 0x1c2);
}

#[test]
fn test_KEY_DEL_LINE() {
	assert_eq!(KEY_DEL_LINE!(), 0x1c3);
}

#[test]
fn test_KEY_FN() {
	assert_eq!(KEY_FN!(), 0x1d0);
}

#[test]
fn test_KEY_FN_ESC() {
	assert_eq!(KEY_FN_ESC!(), 0x1d1);
}

#[test]
fn test_KEY_FN_F1() {
	assert_eq!(KEY_FN_F1!(), 0x1d2);
}

#[test]
fn test_KEY_FN_F2() {
	assert_eq!(KEY_FN_F2!(), 0x1d3);
}

#[test]
fn test_KEY_FN_F3() {
	assert_eq!(KEY_FN_F3!(), 0x1d4);
}

#[test]
fn test_KEY_FN_F4() {
	assert_eq!(KEY_FN_F4!(), 0x1d5);
}

#[test]
fn test_KEY_FN_F5() {
	assert_eq!(KEY_FN_F5!(), 0x1d6);
}

#[test]
fn test_KEY_FN_F6() {
	assert_eq!(KEY_FN_F6!(), 0x1d7);
}

#[test]
fn test_KEY_FN_F7() {
	assert_eq!(KEY_FN_F7!(), 0x1d8);
}

#[test]
fn test_KEY_FN_F8() {
	assert_eq!(KEY_FN_F8!(), 0x1d9);
}

#[test]
fn test_KEY_FN_F9() {
	assert_eq!(KEY_FN_F9!(), 0x1da);
}

#[test]
fn test_KEY_FN_F10() {
	assert_eq!(KEY_FN_F10!(), 0x1db);
}

#[test]
fn test_KEY_FN_F11() {
	assert_eq!(KEY_FN_F11!(), 0x1dc);
}

#[test]
fn test_KEY_FN_F12() {
	assert_eq!(KEY_FN_F12!(), 0x1dd);
}

#[test]
fn test_KEY_FN_1() {
	assert_eq!(KEY_FN_1!(), 0x1de);
}

#[test]
fn test_KEY_FN_2() {
	assert_eq!(KEY_FN_2!(), 0x1df);
}

#[test]
fn test_KEY_FN_D() {
	assert_eq!(KEY_FN_D!(), 0x1e0);
}

#[test]
fn test_KEY_FN_E() {
	assert_eq!(KEY_FN_E!(), 0x1e1);
}

#[test]
fn test_KEY_FN_F() {
	assert_eq!(KEY_FN_F!(), 0x1e2);
}

#[test]
fn test_KEY_FN_S() {
	assert_eq!(KEY_FN_S!(), 0x1e3);
}

#[test]
fn test_KEY_FN_B() {
	assert_eq!(KEY_FN_B!(), 0x1e4);
}

#[test]
fn test_KEY_FN_RIGHT_SHIFT() {
	assert_eq!(KEY_FN_RIGHT_SHIFT!(), 0x1e5);
}

#[test]
fn test_KEY_BRL_DOT1() {
	assert_eq!(KEY_BRL_DOT1!(), 0x1f1);
}

#[test]
fn test_KEY_BRL_DOT2() {
	assert_eq!(KEY_BRL_DOT2!(), 0x1f2);
}

#[test]
fn test_KEY_BRL_DOT3() {
	assert_eq!(KEY_BRL_DOT3!(), 0x1f3);
}

#[test]
fn test_KEY_BRL_DOT4() {
	assert_eq!(KEY_BRL_DOT4!(), 0x1f4);
}

#[test]
fn test_KEY_BRL_DOT5() {
	assert_eq!(KEY_BRL_DOT5!(), 0x1f5);
}

#[test]
fn test_KEY_BRL_DOT6() {
	assert_eq!(KEY_BRL_DOT6!(), 0x1f6);
}

#[test]
fn test_KEY_BRL_DOT7() {
	assert_eq!(KEY_BRL_DOT7!(), 0x1f7);
}

#[test]
fn test_KEY_BRL_DOT8() {
	assert_eq!(KEY_BRL_DOT8!(), 0x1f8);
}

#[test]
fn test_KEY_BRL_DOT9() {
	assert_eq!(KEY_BRL_DOT9!(), 0x1f9);
}

#[test]
fn test_KEY_BRL_DOT10() {
	assert_eq!(KEY_BRL_DOT10!(), 0x1fa);
}

#[test]
fn test_KEY_NUMERIC_0() {
	assert_eq!(KEY_NUMERIC_0!(), 0x200);
}

#[test]
fn test_KEY_NUMERIC_1() {
	assert_eq!(KEY_NUMERIC_1!(), 0x201);
}

#[test]
fn test_KEY_NUMERIC_2() {
	assert_eq!(KEY_NUMERIC_2!(), 0x202);
}

#[test]
fn test_KEY_NUMERIC_3() {
	assert_eq!(KEY_NUMERIC_3!(), 0x203);
}

#[test]
fn test_KEY_NUMERIC_4() {
	assert_eq!(KEY_NUMERIC_4!(), 0x204);
}

#[test]
fn test_KEY_NUMERIC_5() {
	assert_eq!(KEY_NUMERIC_5!(), 0x205);
}

#[test]
fn test_KEY_NUMERIC_6() {
	assert_eq!(KEY_NUMERIC_6!(), 0x206);
}

#[test]
fn test_KEY_NUMERIC_7() {
	assert_eq!(KEY_NUMERIC_7!(), 0x207);
}

#[test]
fn test_KEY_NUMERIC_8() {
	assert_eq!(KEY_NUMERIC_8!(), 0x208);
}

#[test]
fn test_KEY_NUMERIC_9() {
	assert_eq!(KEY_NUMERIC_9!(), 0x209);
}

#[test]
fn test_KEY_NUMERIC_STAR() {
	assert_eq!(KEY_NUMERIC_STAR!(), 0x20a);
}

#[test]
fn test_KEY_NUMERIC_POUND() {
	assert_eq!(KEY_NUMERIC_POUND!(), 0x20b);
}

#[test]
fn test_KEY_NUMERIC_A() {
	assert_eq!(KEY_NUMERIC_A!(), 0x20c);
}

#[test]
fn test_KEY_NUMERIC_B() {
	assert_eq!(KEY_NUMERIC_B!(), 0x20d);
}

#[test]
fn test_KEY_NUMERIC_C() {
	assert_eq!(KEY_NUMERIC_C!(), 0x20e);
}

#[test]
fn test_KEY_NUMERIC_D() {
	assert_eq!(KEY_NUMERIC_D!(), 0x20f);
}

#[test]
fn test_KEY_CAMERA_FOCUS() {
	assert_eq!(KEY_CAMERA_FOCUS!(), 0x210);
}

#[test]
fn test_KEY_WPS_BUTTON() {
	assert_eq!(KEY_WPS_BUTTON!(), 0x211);
}

#[test]
fn test_KEY_TOUCHPAD_TOGGLE() {
	assert_eq!(KEY_TOUCHPAD_TOGGLE!(), 0x212);
}

#[test]
fn test_KEY_TOUCHPAD_ON() {
	assert_eq!(KEY_TOUCHPAD_ON!(), 0x213);
}

#[test]
fn test_KEY_TOUCHPAD_OFF() {
	assert_eq!(KEY_TOUCHPAD_OFF!(), 0x214);
}

#[test]
fn test_KEY_CAMERA_ZOOMIN() {
	assert_eq!(KEY_CAMERA_ZOOMIN!(), 0x215);
}

#[test]
fn test_KEY_CAMERA_ZOOMOUT() {
	assert_eq!(KEY_CAMERA_ZOOMOUT!(), 0x216);
}

#[test]
fn test_KEY_CAMERA_UP() {
	assert_eq!(KEY_CAMERA_UP!(), 0x217);
}

#[test]
fn test_KEY_CAMERA_DOWN() {
	assert_eq!(KEY_CAMERA_DOWN!(), 0x218);
}

#[test]
fn test_KEY_CAMERA_LEFT() {
	assert_eq!(KEY_CAMERA_LEFT!(), 0x219);
}

#[test]
fn test_KEY_CAMERA_RIGHT() {
	assert_eq!(KEY_CAMERA_RIGHT!(), 0x21a);
}

#[test]
fn test_KEY_ATTENDANT_ON() {
	assert_eq!(KEY_ATTENDANT_ON!(), 0x21b);
}

#[test]
fn test_KEY_ATTENDANT_OFF() {
	assert_eq!(KEY_ATTENDANT_OFF!(), 0x21c);
}

#[test]
fn test_KEY_ATTENDANT_TOGGLE() {
	assert_eq!(KEY_ATTENDANT_TOGGLE!(), 0x21d);
}

#[test]
fn test_KEY_LIGHTS_TOGGLE() {
	assert_eq!(KEY_LIGHTS_TOGGLE!(), 0x21e);
}

#[test]
fn test_BTN_DPAD_UP() {
	assert_eq!(BTN_DPAD_UP!(), 0x220);
}

#[test]
fn test_BTN_DPAD_DOWN() {
	assert_eq!(BTN_DPAD_DOWN!(), 0x221);
}

#[test]
fn test_BTN_DPAD_LEFT() {
	assert_eq!(BTN_DPAD_LEFT!(), 0x222);
}

#[test]
fn test_BTN_DPAD_RIGHT() {
	assert_eq!(BTN_DPAD_RIGHT!(), 0x223);
}

#[test]
fn test_KEY_ALS_TOGGLE() {
	assert_eq!(KEY_ALS_TOGGLE!(), 0x230);
}

#[test]
fn test_KEY_ROTATE_LOCK_TOGGLE() {
	assert_eq!(KEY_ROTATE_LOCK_TOGGLE!(), 0x231);
}

#[test]
fn test_KEY_BUTTONCONFIG() {
	assert_eq!(KEY_BUTTONCONFIG!(), 0x240);
}

#[test]
fn test_KEY_TASKMANAGER() {
	assert_eq!(KEY_TASKMANAGER!(), 0x241);
}

#[test]
fn test_KEY_JOURNAL() {
	assert_eq!(KEY_JOURNAL!(), 0x242);
}

#[test]
fn test_KEY_CONTROLPANEL() {
	assert_eq!(KEY_CONTROLPANEL!(), 0x243);
}

#[test]
fn test_KEY_APPSELECT() {
	assert_eq!(KEY_APPSELECT!(), 0x244);
}

#[test]
fn test_KEY_SCREENSAVER() {
	assert_eq!(KEY_SCREENSAVER!(), 0x245);
}

#[test]
fn test_KEY_VOICECOMMAND() {
	assert_eq!(KEY_VOICECOMMAND!(), 0x246);
}

#[test]
fn test_KEY_ASSISTANT() {
	assert_eq!(KEY_ASSISTANT!(), 0x247);
}

#[test]
fn test_KEY_KBD_LAYOUT_NEXT() {
	assert_eq!(KEY_KBD_LAYOUT_NEXT!(), 0x248);
}

#[test]
fn test_KEY_EMOJI_PICKER() {
	assert_eq!(KEY_EMOJI_PICKER!(), 0x249);
}

#[test]
fn test_KEY_DICTATE() {
	assert_eq!(KEY_DICTATE!(), 0x24a);
}

#[test]
fn test_KEY_CAMERA_ACCESS_ENABLE() {
	assert_eq!(KEY_CAMERA_ACCESS_ENABLE!(), 0x24b);
}

#[test]
fn test_KEY_CAMERA_ACCESS_DISABLE() {
	assert_eq!(KEY_CAMERA_ACCESS_DISABLE!(), 0x24c);
}

#[test]
fn test_KEY_CAMERA_ACCESS_TOGGLE() {
	assert_eq!(KEY_CAMERA_ACCESS_TOGGLE!(), 0x24d);
}

#[test]
fn test_KEY_BRIGHTNESS_MIN() {
	assert_eq!(KEY_BRIGHTNESS_MIN!(), 0x250);
}

#[test]
fn test_KEY_BRIGHTNESS_MAX() {
	assert_eq!(KEY_BRIGHTNESS_MAX!(), 0x251);
}

#[test]
fn test_KEY_KBDINPUTASSIST_PREV() {
	assert_eq!(KEY_KBDINPUTASSIST_PREV!(), 0x260);
}

#[test]
fn test_KEY_KBDINPUTASSIST_NEXT() {
	assert_eq!(KEY_KBDINPUTASSIST_NEXT!(), 0x261);
}

#[test]
fn test_KEY_KBDINPUTASSIST_PREVGROUP() {
	assert_eq!(KEY_KBDINPUTASSIST_PREVGROUP!(), 0x262);
}

#[test]
fn test_KEY_KBDINPUTASSIST_NEXTGROUP() {
	assert_eq!(KEY_KBDINPUTASSIST_NEXTGROUP!(), 0x263);
}

#[test]
fn test_KEY_KBDINPUTASSIST_ACCEPT() {
	assert_eq!(KEY_KBDINPUTASSIST_ACCEPT!(), 0x264);
}

#[test]
fn test_KEY_KBDINPUTASSIST_CANCEL() {
	assert_eq!(KEY_KBDINPUTASSIST_CANCEL!(), 0x265);
}

#[test]
fn test_KEY_RIGHT_UP() {
	assert_eq!(KEY_RIGHT_UP!(), 0x266);
}

#[test]
fn test_KEY_RIGHT_DOWN() {
	assert_eq!(KEY_RIGHT_DOWN!(), 0x267);
}

#[test]
fn test_KEY_LEFT_UP() {
	assert_eq!(KEY_LEFT_UP!(), 0x268);
}

#[test]
fn test_KEY_LEFT_DOWN() {
	assert_eq!(KEY_LEFT_DOWN!(), 0x269);
}

#[test]
fn test_KEY_ROOT_MENU() {
	assert_eq!(KEY_ROOT_MENU!(), 0x26a);
}

#[test]
fn test_KEY_MEDIA_TOP_MENU() {
	assert_eq!(KEY_MEDIA_TOP_MENU!(), 0x26b);
}

#[test]
fn test_KEY_NUMERIC_11() {
	assert_eq!(KEY_NUMERIC_11!(), 0x26c);
}

#[test]
fn test_KEY_NUMERIC_12() {
	assert_eq!(KEY_NUMERIC_12!(), 0x26d);
}

#[test]
fn test_KEY_AUDIO_DESC() {
	assert_eq!(KEY_AUDIO_DESC!(), 0x26e);
}

#[test]
fn test_KEY_3D_MODE() {
	assert_eq!(KEY_3D_MODE!(), 0x26f);
}

#[test]
fn test_KEY_NEXT_FAVORITE() {
	assert_eq!(KEY_NEXT_FAVORITE!(), 0x270);
}

#[test]
fn test_KEY_STOP_RECORD() {
	assert_eq!(KEY_STOP_RECORD!(), 0x271);
}

#[test]
fn test_KEY_PAUSE_RECORD() {
	assert_eq!(KEY_PAUSE_RECORD!(), 0x272);
}

#[test]
fn test_KEY_VOD() {
	assert_eq!(KEY_VOD!(), 0x273);
}

#[test]
fn test_KEY_UNMUTE() {
	assert_eq!(KEY_UNMUTE!(), 0x274);
}

#[test]
fn test_KEY_FASTREVERSE() {
	assert_eq!(KEY_FASTREVERSE!(), 0x275);
}

#[test]
fn test_KEY_SLOWREVERSE() {
	assert_eq!(KEY_SLOWREVERSE!(), 0x276);
}

#[test]
fn test_KEY_DATA() {
	assert_eq!(KEY_DATA!(), 0x277);
}

#[test]
fn test_KEY_ONSCREEN_KEYBOARD() {
	assert_eq!(KEY_ONSCREEN_KEYBOARD!(), 0x278);
}

#[test]
fn test_KEY_PRIVACY_SCREEN_TOGGLE() {
	assert_eq!(KEY_PRIVACY_SCREEN_TOGGLE!(), 0x279);
}

#[test]
fn test_KEY_SELECTIVE_SCREENSHOT() {
	assert_eq!(KEY_SELECTIVE_SCREENSHOT!(), 0x27a);
}

#[test]
fn test_KEY_NEXT_ELEMENT() {
	assert_eq!(KEY_NEXT_ELEMENT!(), 0x27b);
}

#[test]
fn test_KEY_PREVIOUS_ELEMENT() {
	assert_eq!(KEY_PREVIOUS_ELEMENT!(), 0x27c);
}

#[test]
fn test_KEY_AUTOPILOT_ENGAGE_TOGGLE() {
	assert_eq!(KEY_AUTOPILOT_ENGAGE_TOGGLE!(), 0x27d);
}

#[test]
fn test_KEY_MARK_WAYPOINT() {
	assert_eq!(KEY_MARK_WAYPOINT!(), 0x27e);
}

#[test]
fn test_KEY_SOS() {
	assert_eq!(KEY_SOS!(), 0x27f);
}

#[test]
fn test_KEY_NAV_CHART() {
	assert_eq!(KEY_NAV_CHART!(), 0x280);
}

#[test]
fn test_KEY_FISHING_CHART() {
	assert_eq!(KEY_FISHING_CHART!(), 0x281);
}

#[test]
fn test_KEY_SINGLE_RANGE_RADAR() {
	assert_eq!(KEY_SINGLE_RANGE_RADAR!(), 0x282);
}

#[test]
fn test_KEY_DUAL_RANGE_RADAR() {
	assert_eq!(KEY_DUAL_RANGE_RADAR!(), 0x283);
}

#[test]
fn test_KEY_RADAR_OVERLAY() {
	assert_eq!(KEY_RADAR_OVERLAY!(), 0x284);
}

#[test]
fn test_KEY_TRADITIONAL_SONAR() {
	assert_eq!(KEY_TRADITIONAL_SONAR!(), 0x285);
}

#[test]
fn test_KEY_CLEARVU_SONAR() {
	assert_eq!(KEY_CLEARVU_SONAR!(), 0x286);
}

#[test]
fn test_KEY_SIDEVU_SONAR() {
	assert_eq!(KEY_SIDEVU_SONAR!(), 0x287);
}

#[test]
fn test_KEY_NAV_INFO() {
	assert_eq!(KEY_NAV_INFO!(), 0x288);
}

#[test]
fn test_KEY_BRIGHTNESS_MENU() {
	assert_eq!(KEY_BRIGHTNESS_MENU!(), 0x289);
}

#[test]
fn test_KEY_MACRO1() {
	assert_eq!(KEY_MACRO1!(), 0x290);
}

#[test]
fn test_KEY_MACRO2() {
	assert_eq!(KEY_MACRO2!(), 0x291);
}

#[test]
fn test_KEY_MACRO3() {
	assert_eq!(KEY_MACRO3!(), 0x292);
}

#[test]
fn test_KEY_MACRO4() {
	assert_eq!(KEY_MACRO4!(), 0x293);
}

#[test]
fn test_KEY_MACRO5() {
	assert_eq!(KEY_MACRO5!(), 0x294);
}

#[test]
fn test_KEY_MACRO6() {
	assert_eq!(KEY_MACRO6!(), 0x295);
}

#[test]
fn test_KEY_MACRO7() {
	assert_eq!(KEY_MACRO7!(), 0x296);
}

#[test]
fn test_KEY_MACRO8() {
	assert_eq!(KEY_MACRO8!(), 0x297);
}

#[test]
fn test_KEY_MACRO9() {
	assert_eq!(KEY_MACRO9!(), 0x298);
}

#[test]
fn test_KEY_MACRO10() {
	assert_eq!(KEY_MACRO10!(), 0x299);
}

#[test]
fn test_KEY_MACRO11() {
	assert_eq!(KEY_MACRO11!(), 0x29a);
}

#[test]
fn test_KEY_MACRO12() {
	assert_eq!(KEY_MACRO12!(), 0x29b);
}

#[test]
fn test_KEY_MACRO13() {
	assert_eq!(KEY_MACRO13!(), 0x29c);
}

#[test]
fn test_KEY_MACRO14() {
	assert_eq!(KEY_MACRO14!(), 0x29d);
}

#[test]
fn test_KEY_MACRO15() {
	assert_eq!(KEY_MACRO15!(), 0x29e);
}

#[test]
fn test_KEY_MACRO16() {
	assert_eq!(KEY_MACRO16!(), 0x29f);
}

#[test]
fn test_KEY_MACRO17() {
	assert_eq!(KEY_MACRO17!(), 0x2a0);
}

#[test]
fn test_KEY_MACRO18() {
	assert_eq!(KEY_MACRO18!(), 0x2a1);
}

#[test]
fn test_KEY_MACRO19() {
	assert_eq!(KEY_MACRO19!(), 0x2a2);
}

#[test]
fn test_KEY_MACRO20() {
	assert_eq!(KEY_MACRO20!(), 0x2a3);
}

#[test]
fn test_KEY_MACRO21() {
	assert_eq!(KEY_MACRO21!(), 0x2a4);
}

#[test]
fn test_KEY_MACRO22() {
	assert_eq!(KEY_MACRO22!(), 0x2a5);
}

#[test]
fn test_KEY_MACRO23() {
	assert_eq!(KEY_MACRO23!(), 0x2a6);
}

#[test]
fn test_KEY_MACRO24() {
	assert_eq!(KEY_MACRO24!(), 0x2a7);
}

#[test]
fn test_KEY_MACRO25() {
	assert_eq!(KEY_MACRO25!(), 0x2a8);
}

#[test]
fn test_KEY_MACRO26() {
	assert_eq!(KEY_MACRO26!(), 0x2a9);
}

#[test]
fn test_KEY_MACRO27() {
	assert_eq!(KEY_MACRO27!(), 0x2aa);
}

#[test]
fn test_KEY_MACRO28() {
	assert_eq!(KEY_MACRO28!(), 0x2ab);
}

#[test]
fn test_KEY_MACRO29() {
	assert_eq!(KEY_MACRO29!(), 0x2ac);
}

#[test]
fn test_KEY_MACRO30() {
	assert_eq!(KEY_MACRO30!(), 0x2ad);
}

#[test]
fn test_KEY_MACRO_RECORD_START() {
	assert_eq!(KEY_MACRO_RECORD_START!(), 0x2b0);
}

#[test]
fn test_KEY_MACRO_RECORD_STOP() {
	assert_eq!(KEY_MACRO_RECORD_STOP!(), 0x2b1);
}

#[test]
fn test_KEY_MACRO_PRESET_CYCLE() {
	assert_eq!(KEY_MACRO_PRESET_CYCLE!(), 0x2b2);
}

#[test]
fn test_KEY_MACRO_PRESET1() {
	assert_eq!(KEY_MACRO_PRESET1!(), 0x2b3);
}

#[test]
fn test_KEY_MACRO_PRESET2() {
	assert_eq!(KEY_MACRO_PRESET2!(), 0x2b4);
}

#[test]
fn test_KEY_MACRO_PRESET3() {
	assert_eq!(KEY_MACRO_PRESET3!(), 0x2b5);
}

#[test]
fn test_KEY_KBD_LCD_MENU1() {
	assert_eq!(KEY_KBD_LCD_MENU1!(), 0x2b8);
}

#[test]
fn test_KEY_KBD_LCD_MENU2() {
	assert_eq!(KEY_KBD_LCD_MENU2!(), 0x2b9);
}

#[test]
fn test_KEY_KBD_LCD_MENU3() {
	assert_eq!(KEY_KBD_LCD_MENU3!(), 0x2ba);
}

#[test]
fn test_KEY_KBD_LCD_MENU4() {
	assert_eq!(KEY_KBD_LCD_MENU4!(), 0x2bb);
}

#[test]
fn test_KEY_KBD_LCD_MENU5() {
	assert_eq!(KEY_KBD_LCD_MENU5!(), 0x2bc);
}

#[test]
fn test_BTN_TRIGGER_HAPPY() {
	assert_eq!(BTN_TRIGGER_HAPPY!(), 0x2c0);
}

#[test]
fn test_BTN_TRIGGER_HAPPY1() {
	assert_eq!(BTN_TRIGGER_HAPPY1!(), 0x2c0);
}

#[test]
fn test_BTN_TRIGGER_HAPPY2() {
	assert_eq!(BTN_TRIGGER_HAPPY2!(), 0x2c1);
}

#[test]
fn test_BTN_TRIGGER_HAPPY3() {
	assert_eq!(BTN_TRIGGER_HAPPY3!(), 0x2c2);
}

#[test]
fn test_BTN_TRIGGER_HAPPY4() {
	assert_eq!(BTN_TRIGGER_HAPPY4!(), 0x2c3);
}

#[test]
fn test_BTN_TRIGGER_HAPPY5() {
	assert_eq!(BTN_TRIGGER_HAPPY5!(), 0x2c4);
}

#[test]
fn test_BTN_TRIGGER_HAPPY6() {
	assert_eq!(BTN_TRIGGER_HAPPY6!(), 0x2c5);
}

#[test]
fn test_BTN_TRIGGER_HAPPY7() {
	assert_eq!(BTN_TRIGGER_HAPPY7!(), 0x2c6);
}

#[test]
fn test_BTN_TRIGGER_HAPPY8() {
	assert_eq!(BTN_TRIGGER_HAPPY8!(), 0x2c7);
}

#[test]
fn test_BTN_TRIGGER_HAPPY9() {
	assert_eq!(BTN_TRIGGER_HAPPY9!(), 0x2c8);
}

#[test]
fn test_BTN_TRIGGER_HAPPY10() {
	assert_eq!(BTN_TRIGGER_HAPPY10!(), 0x2c9);
}

#[test]
fn test_BTN_TRIGGER_HAPPY11() {
	assert_eq!(BTN_TRIGGER_HAPPY11!(), 0x2ca);
}

#[test]
fn test_BTN_TRIGGER_HAPPY12() {
	assert_eq!(BTN_TRIGGER_HAPPY12!(), 0x2cb);
}

#[test]
fn test_BTN_TRIGGER_HAPPY13() {
	assert_eq!(BTN_TRIGGER_HAPPY13!(), 0x2cc);
}

#[test]
fn test_BTN_TRIGGER_HAPPY14() {
	assert_eq!(BTN_TRIGGER_HAPPY14!(), 0x2cd);
}

#[test]
fn test_BTN_TRIGGER_HAPPY15() {
	assert_eq!(BTN_TRIGGER_HAPPY15!(), 0x2ce);
}

#[test]
fn test_BTN_TRIGGER_HAPPY16() {
	assert_eq!(BTN_TRIGGER_HAPPY16!(), 0x2cf);
}

#[test]
fn test_BTN_TRIGGER_HAPPY17() {
	assert_eq!(BTN_TRIGGER_HAPPY17!(), 0x2d0);
}

#[test]
fn test_BTN_TRIGGER_HAPPY18() {
	assert_eq!(BTN_TRIGGER_HAPPY18!(), 0x2d1);
}

#[test]
fn test_BTN_TRIGGER_HAPPY19() {
	assert_eq!(BTN_TRIGGER_HAPPY19!(), 0x2d2);
}

#[test]
fn test_BTN_TRIGGER_HAPPY20() {
	assert_eq!(BTN_TRIGGER_HAPPY20!(), 0x2d3);
}

#[test]
fn test_BTN_TRIGGER_HAPPY21() {
	assert_eq!(BTN_TRIGGER_HAPPY21!(), 0x2d4);
}

#[test]
fn test_BTN_TRIGGER_HAPPY22() {
	assert_eq!(BTN_TRIGGER_HAPPY22!(), 0x2d5);
}

#[test]
fn test_BTN_TRIGGER_HAPPY23() {
	assert_eq!(BTN_TRIGGER_HAPPY23!(), 0x2d6);
}

#[test]
fn test_BTN_TRIGGER_HAPPY24() {
	assert_eq!(BTN_TRIGGER_HAPPY24!(), 0x2d7);
}

#[test]
fn test_BTN_TRIGGER_HAPPY25() {
	assert_eq!(BTN_TRIGGER_HAPPY25!(), 0x2d8);
}

#[test]
fn test_BTN_TRIGGER_HAPPY26() {
	assert_eq!(BTN_TRIGGER_HAPPY26!(), 0x2d9);
}

#[test]
fn test_BTN_TRIGGER_HAPPY27() {
	assert_eq!(BTN_TRIGGER_HAPPY27!(), 0x2da);
}

#[test]
fn test_BTN_TRIGGER_HAPPY28() {
	assert_eq!(BTN_TRIGGER_HAPPY28!(), 0x2db);
}

#[test]
fn test_BTN_TRIGGER_HAPPY29() {
	assert_eq!(BTN_TRIGGER_HAPPY29!(), 0x2dc);
}

#[test]
fn test_BTN_TRIGGER_HAPPY30() {
	assert_eq!(BTN_TRIGGER_HAPPY30!(), 0x2dd);
}

#[test]
fn test_BTN_TRIGGER_HAPPY31() {
	assert_eq!(BTN_TRIGGER_HAPPY31!(), 0x2de);
}

#[test]
fn test_BTN_TRIGGER_HAPPY32() {
	assert_eq!(BTN_TRIGGER_HAPPY32!(), 0x2df);
}

#[test]
fn test_BTN_TRIGGER_HAPPY33() {
	assert_eq!(BTN_TRIGGER_HAPPY33!(), 0x2e0);
}

#[test]
fn test_BTN_TRIGGER_HAPPY34() {
	assert_eq!(BTN_TRIGGER_HAPPY34!(), 0x2e1);
}

#[test]
fn test_BTN_TRIGGER_HAPPY35() {
	assert_eq!(BTN_TRIGGER_HAPPY35!(), 0x2e2);
}

#[test]
fn test_BTN_TRIGGER_HAPPY36() {
	assert_eq!(BTN_TRIGGER_HAPPY36!(), 0x2e3);
}

#[test]
fn test_BTN_TRIGGER_HAPPY37() {
	assert_eq!(BTN_TRIGGER_HAPPY37!(), 0x2e4);
}

#[test]
fn test_BTN_TRIGGER_HAPPY38() {
	assert_eq!(BTN_TRIGGER_HAPPY38!(), 0x2e5);
}

#[test]
fn test_BTN_TRIGGER_HAPPY39() {
	assert_eq!(BTN_TRIGGER_HAPPY39!(), 0x2e6);
}

#[test]
fn test_BTN_TRIGGER_HAPPY40() {
	assert_eq!(BTN_TRIGGER_HAPPY40!(), 0x2e7);
}

#[test]
fn test_KEY_MIN_INTERESTING() {
	assert_eq!(KEY_MIN_INTERESTING!(), KEY_MUTE!());
}

#[test]
fn test_KEY_MAX() {
	assert_eq!(KEY_MAX!(), 0x2ff);
}

#[test]
fn test_KEY_CNT() {
	assert_eq!(KEY_CNT!(), (KEY_MAX!() + 1));
}
