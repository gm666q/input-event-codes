#![allow(non_snake_case)]

use input_event_codes::{SND_BELL, SND_CLICK, SND_CNT, SND_MAX, SND_TONE};

#[test]
fn test_SND_CLICK() {
	assert_eq!(SND_CLICK!(), 0x00);
}

#[test]
fn test_SND_BELL() {
	assert_eq!(SND_BELL!(), 0x01);
}

#[test]
fn test_SND_TONE() {
	assert_eq!(SND_TONE!(), 0x02);
}

#[test]
fn test_SND_MAX() {
	assert_eq!(SND_MAX!(), 0x07);
}

#[test]
fn test_SND_CNT() {
	assert_eq!(SND_CNT!(), (SND_MAX!() + 1));
}
