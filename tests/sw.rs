#![allow(non_snake_case)]

use input_event_codes::{
	SW_CAMERA_LENS_COVER, SW_CNT, SW_DOCK, SW_FRONT_PROXIMITY, SW_HEADPHONE_INSERT,
	SW_JACK_PHYSICAL_INSERT, SW_KEYPAD_SLIDE, SW_LID, SW_LINEIN_INSERT, SW_LINEOUT_INSERT,
	SW_MACHINE_COVER, SW_MAX, SW_MICROPHONE_INSERT, SW_MUTE_DEVICE, SW_PEN_INSERTED, SW_RFKILL_ALL,
	SW_ROTATE_LOCK, SW_TABLET_MODE, SW_VIDEOOUT_INSERT,
};

#[test]
fn test_SW_LID() {
	assert_eq!(SW_LID!(), 0x00);
}

#[test]
fn test_SW_TABLET_MODE() {
	assert_eq!(SW_TABLET_MODE!(), 0x01);
}

#[test]
fn test_SW_HEADPHONE_INSERT() {
	assert_eq!(SW_HEADPHONE_INSERT!(), 0x02);
}

#[test]
fn test_SW_RFKILL_ALL() {
	assert_eq!(SW_RFKILL_ALL!(), 0x03);
}

#[test]
fn test_SW_MICROPHONE_INSERT() {
	assert_eq!(SW_MICROPHONE_INSERT!(), 0x04);
}

#[test]
fn test_SW_DOCK() {
	assert_eq!(SW_DOCK!(), 0x05);
}

#[test]
fn test_SW_LINEOUT_INSERT() {
	assert_eq!(SW_LINEOUT_INSERT!(), 0x06);
}

#[test]
fn test_SW_JACK_PHYSICAL_INSERT() {
	assert_eq!(SW_JACK_PHYSICAL_INSERT!(), 0x07);
}

#[test]
fn test_SW_VIDEOOUT_INSERT() {
	assert_eq!(SW_VIDEOOUT_INSERT!(), 0x08);
}

#[test]
fn test_SW_CAMERA_LENS_COVER() {
	assert_eq!(SW_CAMERA_LENS_COVER!(), 0x09);
}

#[test]
fn test_SW_KEYPAD_SLIDE() {
	assert_eq!(SW_KEYPAD_SLIDE!(), 0x0a);
}

#[test]
fn test_SW_FRONT_PROXIMITY() {
	assert_eq!(SW_FRONT_PROXIMITY!(), 0x0b);
}

#[test]
fn test_SW_ROTATE_LOCK() {
	assert_eq!(SW_ROTATE_LOCK!(), 0x0c);
}

#[test]
fn test_SW_LINEIN_INSERT() {
	assert_eq!(SW_LINEIN_INSERT!(), 0x0d);
}

#[test]
fn test_SW_MUTE_DEVICE() {
	assert_eq!(SW_MUTE_DEVICE!(), 0x0e);
}

#[test]
fn test_SW_PEN_INSERTED() {
	assert_eq!(SW_PEN_INSERTED!(), 0x0f);
}

#[test]
fn test_SW_MACHINE_COVER() {
	assert_eq!(SW_MACHINE_COVER!(), 0x10);
}

#[test]
fn test_SW_MAX() {
	assert_eq!(SW_MAX!(), 0x10);
}

#[test]
fn test_SW_CNT() {
	assert_eq!(SW_CNT!(), (SW_MAX!() + 1));
}
